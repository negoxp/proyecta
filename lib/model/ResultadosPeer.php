<?php


/**
 * Skeleton subclass for performing query and update operations on the 'resultados' table.
 *
 * 
 *
 * This class was autogenerated by Propel 1.4.2 on:
 *
 * Mon May 11 11:15:52 2015
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ResultadosPeer extends BaseResultadosPeer {

} // ResultadosPeer
