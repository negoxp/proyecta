<?php

/**
 * Project form base class.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
abstract class BaseFormPropel extends sfFormPropel
{
  public function setup()
  {
  }
}
