<?php

/**
 * Sms form.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
class SmsForm extends BaseSmsForm
{
  public function configure()
  {
  
  foreach ($this->getWidgetSchema()->getFields() as $field)
	{
	  $field->setAttribute('class', 'form-control form-cascade-control');
	}

  }

}
