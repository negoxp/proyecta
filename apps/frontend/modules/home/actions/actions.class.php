<?php

/**
 * home actions.
 *
 * @package    micros
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
    $this->redirect('home/log');
    /*
    $c=new Criteria(); 
    //$c->addand(SmsPeer::BUSSINES_DATE, $yesterday." 00:00:00", Criteria::EQUAL);
    $c->addDescendingOrderByColumn(SmsPeer::ID);
    $this->smss = SmsPeer::doSelect($c);

    $c=new Criteria(); 
    //$c->addand(SmsPeer::BUSSINES_DATE, $yesterday." 00:00:00", Criteria::EQUAL);
    $c->addDescendingOrderByColumn(ResultadosPeer::ID);
    $this->resultados = ResultadosPeer::doSelect($c);

    */

  }

  public function executeLog(sfWebRequest $request)
  {
    //$this->forward('default', 'module');

    $this->filter = $request->getParameter('filter');
    if ($this->filter!=""){
      $_SESSION["filter"] = $this->filter;  
    }
    if ($this->filter=="all"){
      $_SESSION["filter"] = "";
    }
    //$_SESSION["filter"] = "";

    $c=new Criteria();
    $config = ConfigPeer::doSelectOne($c);

    $this->canprocess = $config->getCanProcess();
    

    $c=new Criteria(); 
    if ($request->getParameter('borrados')=="1"){
      $c->addand(SmsPeer::DELETED, true, Criteria::EQUAL);
    }else{
      $c->addand(SmsPeer::DELETED, false, Criteria::EQUAL);
    }

    if ($request->getParameter('hablar')=="1"){
      $c->addand(SmsPeer::HABLAR, true, Criteria::EQUAL);
    }else{
      $c->addand(SmsPeer::HABLAR, false, Criteria::EQUAL);
    }

    if ($request->getParameter('id')){
      $c->addand(SmsPeer::ID, $request->getParameter('id'), Criteria::EQUAL);
    }else{
      $c->addand(SmsPeer::DATA, @$_SESSION["filter"]."%", Criteria::LIKE);
    }

    $c->addand(SmsPeer::PROCESSED, false, Criteria::EQUAL);

    if ($request->getParameter('chkerror')){
      $c=new Criteria();
      $c->addand(SmsPeer::PROCESSED, false, Criteria::EQUAL);
      $c->addand(SmsPeer::CHECKED, 2, Criteria::EQUAL);
      $c->addand(SmsPeer::HABLAR, false, Criteria::EQUAL);
      $c->addand(SmsPeer::DELETED, false, Criteria::EQUAL);
    }
    if ($request->getParameter('procesados')){
      $c=new Criteria();
      $c->addand(SmsPeer::PROCESSED, true, Criteria::EQUAL);
    }

    if ($request->getParameter('errorlog')=="1"){
      $c=new Criteria();
      $c->addand(SmsPeer::ERROR_LOG, "", Criteria::NOT_EQUAL);
      $c->addand(SmsPeer::DELETED, false, Criteria::EQUAL);
      $c->addand(SmsPeer::HABLAR, false, Criteria::EQUAL);
      $c->addand(SmsPeer::PROCESSED, true, Criteria::EQUAL);
      $c->setLimit(50);
    }

    //$c=new Criteria();

    $c->addDescendingOrderByColumn(SmsPeer::ID); 
    $this->smss = SmsPeer::doSelect($c);

  }

  public function executePalabras(sfWebRequest $request)
  {
    $c=new Criteria(); 
    $c->addGroupByColumn('ESTADO');
    //$c->addand(SmsPeer::BUSSINES_DATE, $yesterday." 00:00:00", Criteria::EQUAL);
    $c->addDescendingOrderByColumn(ResultadosPeer::ESTADO);
    $this->resultados = ResultadosPeer::doSelect($c);
  }

  public function executeSeccionessms(sfWebRequest $request)
  {
    $eid=$request->getParameter('eid');

    $eleccion = EleccionPeer::retrieveByPk($eid);

    $c=new Criteria(); 
    $c->addand(SeccionPeer::PALABRA_ID, $eleccion->getPalabraId() , Criteria::EQUAL);
    $c->addAscendingOrderByColumn(SeccionPeer::ID);
    $this->secciones = SeccionPeer::doSelect($c);

    $this->eleccion = EleccionPeer::retrieveByPk($eid);

  }

  public function executeMandarplantilla(sfWebRequest $request)
  {
    exit();
    $c=new Criteria(); 
    $c->addand(SeccionPeer::PALABRA_ID, 2, Criteria::EQUAL);
    $c->addand(SeccionPeer::MUNICIPIO, "ton", Criteria::EQUAL);
    $secciones = SeccionPeer::doSelect($c);
    foreach ($secciones as $seccion){
      $plantilla = "jal-ton-".$seccion->getSeccion()."-pri-0-mc-0-pan-0-prd-0-pna-0-mor-0-pt-0-ph-0-pes-0-pvem-0-nu-0-nc-0";
      echo $plantilla;
      echo $seccion->getCelEncuestador();
      echo "</br>";
      $celular=$seccion->getCelEncuestador();

      //189.182.173.40
      $result = enviarsms('192.168.1.68','8800','negoxp','Pasion8576', $celular, $plantilla);

      //echo $result;

    }
     exit();

  }

  public function executeEleccion(sfWebRequest $request)
  {
    $pid=$request->getParameter('pid');

    $c=new Criteria(); 
    $c->addAscendingOrderByColumn(EleccionPeer::ID);
    $c->addand(EleccionPeer::PALABRA_ID, $pid, Criteria::EQUAL);
    $this->elecciones = EleccionPeer::doSelect($c);
  }

  public function executeDeleccion(sfWebRequest $request)
  {
    $eid=$request->getParameter('eid');

    $c=new Criteria(); 
    $c->addand(ResultadosPeer::ELECCION_ID, $eid, Criteria::EQUAL);
    if($request->getParameter('prep')==1){
      $c->addand(ResultadosPeer::PREP, TRUE, Criteria::EQUAL);
      $this->nc=false;
    }
    //$c->addand(ResultadosPeer::BORRADO_LOGICO, NULL, Criteria::ISNULL);
    //$c->addand(ResultadosPeer::PREP, 1, Criteria::NOT_EQUAL);
    
    $this->resultados = ResultadosPeer::doSelect($c);

    $this->eleccion = EleccionPeer::retrieveByPk($eid);

   

  }

  public function executeDeleccionsn(sfWebRequest $request)
  {
    $eid=$request->getParameter('eid');

    $c=new Criteria(); 
    $c->addand(ResultadosPeer::ELECCION_ID, $eid, Criteria::EQUAL);
    $c->addand(ResultadosPeer::PREP, TRUE, Criteria::EQUAL);
    //$c->addand(ResultadosPeer::BORRADO_LOGICO, NULL, Criteria::ISNULL);
    $this->resultados = ResultadosPeer::doSelect($c);

    $this->eleccion = EleccionPeer::retrieveByPk($eid);

  }

  public function executeDeleccioncol(sfWebRequest $request)
  {
    $eid=$request->getParameter('eid');
    if($request->getParameter('nc')==1){
      $this->nc=true;
    }else{
      $this->nc=false;
    }

    if($request->getParameter('pv')==1){
      $this->pv=true;
    }else{
      $this->pv=false;
    }

    $c=new Criteria(); 
    $c->addand(ResultadosPeer::ELECCION_ID, $eid, Criteria::EQUAL);
    if($request->getParameter('prep')==1){
      $c->addand(ResultadosPeer::PREP, TRUE, Criteria::EQUAL);
      $this->nc=false;
    }
    //$c->addand(ResultadosPeer::BORRADO_LOGICO, NULL, Criteria::ISNULL);
    //$c->addJoin(ResultadosPeer::SECCION_ID, SeccionPeer::ID);
    $c->addAscendingOrderByColumn(ResultadosPeer::SECCION_ID);

    $this->resultados = ResultadosPeer::doSelect($c);

    $this->eleccion = EleccionPeer::retrieveByPk($eid);

  }

  public function executeDeleccioncol2(sfWebRequest $request)
  {
    $eid=$request->getParameter('eid');

    $c=new Criteria(); 
    $c->addand(ResultadosPeer::ELECCION_ID, $eid, Criteria::EQUAL);
    $c->addand(ResultadosPeer::PREP, TRUE, Criteria::EQUAL);
    //$c->addand(ResultadosPeer::BORRADO_LOGICO, NULL, Criteria::ISNULL);
    //$c->addJoin(ResultadosPeer::SECCION_ID, SeccionPeer::ID);
    $this->resultados = ResultadosPeer::doSelect($c);

    $this->eleccion = EleccionPeer::retrieveByPk($eid);

  }

  public function executeLogicaldelete(sfWebRequest $request)
  {
    
    $smsid=$request->getParameter('smsid');
    $sms = SmsPeer::retrieveByPk($smsid);

    if ($sms->getDeleted()){
      $sms->setDeleted(false);
    }else{
      $sms->setDeleted(true);  
    }

    $sms->save();

    $this->redirect('home/log');

  }

  public function executeHablar(sfWebRequest $request)
  {
    
    $smsid=$request->getParameter('smsid');
    $sms = SmsPeer::retrieveByPk($smsid);
    if ($sms->gethablar()){
      $sms->sethablar(false);
    }else{
      $sms->sethablar(true);  
    }
    
    $sms->save();

    $this->redirect('home/log');

  }

  public function executeSms(sfWebRequest $request)
  {
    
    $smsid=$request->getParameter('smsid');
    $this->sms = SmsPeer::retrieveByPk($smsid);

    $this->smsid=$request->getParameter('smsid');

    //obtener mensajes recibidos.
     $c=new Criteria(); 
     $c->addand(SmsPeer::SENDER, $this->sms->getSender(), Criteria::EQUAL);
     $c->addDescendingOrderByColumn(SmsPeer::ID); 
     $sms_recived = SmsPeer::doSelect($c);

     $i=0;
     foreach ($sms_recived as $sm){
        $hsms[$i]['sms']=$sm->getDataOriginal();
        $hsms[$i]['date']=$sm->getCreatedAt();
        $hsms[$i]['error']=$sm->getErrorLog();
        $hsms[$i]['processed']=$sm->getProcessed();
        $hsms[$i]['tes']="fa-chevron-circle-left blue";
        $i++;
     }

     //obtener mensajes enviados.
     $c=new Criteria(); 
     $c->addand(SmsSenderPeer::TO, $this->sms->getSender(), Criteria::EQUAL);
     //$c->addand(SmsSenderPeer::STATUS, 1, Criteria::EQUAL);
     $c->addDescendingOrderByColumn(SmsSenderPeer::ID); 
     $sms_sent = SmsSenderPeer::doSelect($c);

     foreach ($sms_sent as $sm){
        $hsms[$i]['sms']=$sm->getSms();
        $hsms[$i]['date']=$sm->getCreatedAt();
        $hsms[$i]['error']=$sm->getStatus();
        $hsms[$i]['processed']="";
        $hsms[$i]['tes']="fa-chevron-circle-right green";
        $i++;
     }

     sksort($hsms, $subkey="date", true);

     $this->history=$hsms;

    //$this->redirect('home/log');

  }

  public function executeSendsms(sfWebRequest $request)
  {
    $cel=$request->getParameter('cel');
    $sms=$request->getParameter('sms');
    $smsid=$request->getParameter('smsid');
    $result = enviarsms('192.168.1.71','8800','negoxp','Pasion8576', $cel, $sms);

    //var_dump($result);
    //exit();

    $this->redirect("home/sms?smsid=$smsid");

  }

  public function executeResultado_history(sfWebRequest $request)
  {
    
    $rid=$request->getParameter('rid');
    $this->resultado = ResultadosPeer::retrieveByPk($rid);

  }

  public function executePsms(sfWebRequest $request)
  {
    
    $smsid=$request->getParameter('smsid');
    $res=$this->checksms($smsid);
    $sms = SmsPeer::retrieveByPk($smsid);
    $sms->setErrorLog($res["error_log"]);
    if ($res["error"]){
      $sms->setError($res["error"]);
      $sms->setChecked(2);  
    }
    $sms->save();

    if(!$res["error"]){
      $res["error"]=0;
    }


    $this->redirect('home/log?error='.$res["error"].'&errorlog='.$res["error_log"]." ID: ".$sms->getId());

  }

  public function executeMultiprocess(sfWebRequest $request)
  {

    $asms = $request->getParameter('asms');

    if ( !$asms ){
      $this->redirect('home/log');
    }

    if ($request->getParameter('check') ){
      foreach ($asms as &$smsid) {
        $sms = SmsPeer::retrieveByPk($smsid);
        $res=$this->checksms($smsid, false);
        if ($res["error"]){
          $sms->setChecked(2);
        }else{
          $sms->setChecked(1);
        }
        $sms->save();
      }
    }

    if ($request->getParameter('fix') ){
      foreach ($asms as &$smsid) {
              $sms = SmsPeer::retrieveByPk($smsid);
              $data = $sms->getData();

              //Convertir todo a minusculas
              $data =strtolower($data);

              //remover espacios antes y despues
              $data = trim($data);

              //Sustitucion de palabras
              $data = str_replace(".", "", $data);
              $data = str_replace(",", "", $data);
              $data = str_replace(")", " ", $data);
              $data = str_replace("(", " ", $data);
                $data = str_replace("#", "", $data);

              $data = str_replace("nueva a", "pna", $data);
              $data = str_replace("panal", "pna", $data);
              
              $data = str_replace("movciud", "mc", $data);
              $data = str_replace("mov ciud", "mc", $data);
              $data = str_replace("mc-ciud", "mc", $data);
              $data = str_replace("mccuid", "mc", $data);
              $data = str_replace("movciu", "mc", $data);
              $data = str_replace("moc", "mc", $data);
              $data = str_replace("mov", "mc", $data);


              $data = str_replace("morena", "mor", $data);
              $data = str_replace("moreno", "mor", $data);
              $data = str_replace("moerna", "mor", $data);
              $data = str_replace("mior", "mor", $data);

              $data = str_replace("humanista", "ph", $data);
              $data = str_replace("pph", "ph", $data);
              
              $data = str_replace("encuentro social", "pes", $data);
              $data = str_replace("encuentrosocial", "pes", $data);
              $data = str_replace("encuentro_social", "pes", $data);
              $data = str_replace("esocial", "pes", $data);
              $data = str_replace("ensocial", "pes", $data);
              $data = str_replace("social", "pes", $data);

              $data = str_replace("partidoverde", "pvem", $data);
              $data = str_replace("pverde", "pvem", $data);
              $data = str_replace("verde", "pvem", $data);
              
              $data = str_replace("gol", "gob", $data);

              $data = str_replace("nulos", "nu", $data);
              $data = str_replace("nulo", "nu", $data);

              $data = str_replace("indep", "ind", $data);
              $data = str_replace("inden", "ind", $data);
              $data = str_replace("independiente", "ind", $data);
              $data = str_replace("inp", "ind", $data);

              
              $data = str_replace("rechazados", "nc", $data);
              $data = str_replace("rechazo", "nc", $data);
              
              $data = str_replace("zapopan", "zap", $data);
              $data = str_replace("guadalajara", "gdl", $data);
              $data = str_replace("jalisco", "jal", $data);
              $data = str_replace("colima", "col", $data);
              $data = str_replace("sanfe", "sfr", $data);
              $data = str_replace("-pm10-", "-", $data);
              $data = str_replace("-pm8-", "-", $data);
              $data = str_replace("-pm08-", "-", $data);

              
              

              $data = str_replace("mor-df10", "df10", $data);
              $data = str_replace("mor-df8", "df8", $data);
              $data = str_replace("gdl-dl", "dl", $data);
              $data = str_replace("yuriria", "yur", $data);
              $data = str_replace("-sal-", "-salamanca-", $data);

              $data = str_replace("-un-", "-nu-", $data);
              $data = str_replace("-a-", "a-", $data);
              $data = str_replace("-b-", "b-", $data);
              $data = str_replace("-c-", "c-", $data);
              $data = str_replace("-d-", "d-", $data);
              

              $data = str_replace("secc", "sec", $data);

              $data = str_replace("apaseo", "apa", $data);
              $data = str_replace("-otro-", "-otros-", $data);

              

              $data = str_replace("cambio-de-plantilla-hay-que-usar-esta-:-", "", $data);

              


              $data = str_replace("seccion", "sec", $data);
              $data = str_replace("secion", "sec", $data);
              $data = str_replace("#", "-", $data);
              $data = str_replace("  ", "-", $data);
              $data = str_replace(" ", "-", $data);
              $data = str_replace("_", "-", $data);
              $data = str_replace("--", "-", $data);


              //analisar primer palabra
              $ppalabra = substr($data, 0, 3);

              if ($ppalabra=="zap" or $ppalabra=="gdl" or $ppalabra=="ton" or $ppalabra=="ahu"){
                $data = "jal-".$data;
              }

              if ($ppalabra=="mor"){
                $data = "MICH-".$data;
              }

              //eliminar pm
              $data = str_replace("pm-", "", $data);

              //eliminar dl -> solo para el 14
              #$data = str_replace("-dl-", "-dl14-", $data);
              $sms->setData($data);
              $sms->save();

              //cehck
              $res=$this->checksms($smsid, false);
              if ($res["error"]){
                $sms->setChecked(2);
              }else{
                $sms->setChecked(1);
              }
              $sms->save();



      }
    }

    if ($request->getParameter('process') ){
      //echo "Vamos a Process";
      foreach ($asms as &$smsid) {
          $sms = SmsPeer::retrieveByPk($smsid);
          $res=$this->checksms($sms->getId(), true);
          if ($res["error"]){
            $sms->setError($res["error"]);
            $sms->setErrorLog($res["error_log"]);
            $sms->setChecked(2);
            $sms->save();
          }

      }
    }

    $this->redirect('home/log');
     
  }

  public function executeCheckallsms(sfWebRequest $request)
  {

    $c=new Criteria(); 
    //$c->addand(ResultadosPeer::ELECCION_ID, $eid, Criteria::EQUAL);
    $c->addand(SmsPeer::PROCESSED, false, Criteria::EQUAL);
    $c->addand(SmsPeer::DATA, @$_SESSION["filter"]."%", Criteria::LIKE);
    $smss = SmsPeer::doSelect($c);

    foreach ($smss as $sms){
      $res=$this->checksms($sms->getId(), false);
      if ($res["error"]){
        $sms->setChecked(2);
      }else{
        $sms->setChecked(1);
      }
      $sms->save();
    }

    $this->redirect('home/log');

  }

  public function executeFixsms(sfWebRequest $request)
  {
    
    $c=new Criteria(); 
    //$c->addand(SmsPeer::DATA, @$_SESSION["filter"]."%", Criteria::LIKE);
    $c->addand(SmsPeer::PROCESSED, false, Criteria::EQUAL);
    $smss = SmsPeer::doSelect($c);

    foreach ($smss as $sms){
      /*
      $data = $sms->getData();

      //Convertir todo a minusculas
      $data =strtolower($data);

      //remover espacios antes y despues
      $data = trim($data);

      //Sustitucion de palabras
      $data = str_replace(".", "", $data);
      $data = str_replace(",", "", $data);
      $data = str_replace(")", " ", $data);
      $data = str_replace("(", " ", $data);

      $data = str_replace("nueva a", "pna", $data);
      $data = str_replace("panal", "pna", $data);
      
      $data = str_replace("movciud", "mc", $data);
      $data = str_replace("mov ciud", "mc", $data);
      $data = str_replace("mc-ciud", "mc", $data);
      $data = str_replace("mccuid", "mc", $data);
      $data = str_replace("movciu", "mc", $data);
      $data = str_replace("moc", "mc", $data);
      $data = str_replace("mov", "mc", $data);


      $data = str_replace("morena", "mor", $data);
      $data = str_replace("moreno", "mor", $data);
      $data = str_replace("moerna", "mor", $data);
      $data = str_replace("mior", "mor", $data);

      $data = str_replace("humanista", "ph", $data);
      $data = str_replace("pph", "ph", $data);
      
      $data = str_replace("encuentro social", "pes", $data);
      $data = str_replace("encuentrosocial", "pes", $data);
      $data = str_replace("encuentro_social", "pes", $data);
      $data = str_replace("esocial", "pes", $data);
      $data = str_replace("ensocial", "pes", $data);
      $data = str_replace("social", "pes", $data);

      $data = str_replace("partidoverde", "pvem", $data);
      $data = str_replace("pverde", "pvem", $data);
      $data = str_replace("verde", "pvem", $data);
      
      $data = str_replace("gol", "gob", $data);

      $data = str_replace("nulos", "nu", $data);
      $data = str_replace("nulo", "nu", $data);
      
      $data = str_replace("rechazados", "nc", $data);
      $data = str_replace("rechazo", "nc", $data);
      
      $data = str_replace("zapopan", "zap", $data);
      $data = str_replace("guadalajara", "gdl", $data);
      $data = str_replace("jalisco", "jal", $data);
      $data = str_replace("colima", "col", $data);

      
      $data = str_replace("seccion", "", $data);
      $data = str_replace("secion", "", $data);
      $data = str_replace("#", "", $data);
      $data = str_replace("  ", "-", $data);
      $data = str_replace(" ", "-", $data);
      $data = str_replace("_", "-", $data);

      //analisar primer palabra
      $ppalabra = substr($data, 0, 3);

      if ($ppalabra=="zap" or $ppalabra=="gdl" or $ppalabra=="ton" or $ppalabra=="ahu"){
        $data = "jal-".$data;
      }

      //eliminar pm
      $data = str_replace("pm-", "", $data);

      //eliminar dl -> solo para el 14
      #$data = str_replace("-dl-", "-dl14-", $data);
      
      //echo $data;
      //echo "<br/>";
      $sms->setData($data);
      //echo $sms->getData();

      //exit();

      $sms->save();
      */
    }

    $this->redirect('home/log');

  }

    public function executeProcessallsms(sfWebRequest $request)
  {
    
    $c=new Criteria(); 
    $c->addand(SmsPeer::PROCESSED, false, Criteria::EQUAL);
    $c->addand(SmsPeer::HABLAR, false, Criteria::EQUAL);
    $c->addand(SmsPeer::DELETED, false, Criteria::EQUAL);
    $c->addand(SmsPeer::DATA, @$_SESSION["filter"]."%", Criteria::LIKE);
    $c->addAscendingOrderByColumn(SmsPeer::ID);
    $smss = SmsPeer::doSelect($c);

    foreach ($smss as $sms){
      $res=$this->checksms($sms->getId(), true);
      if ($res["error"]){
        $sms->setError($res["error"]);
        $sms->setErrorLog($res["error_log"]);
        $sms->setChecked(2);
        $sms->save();
      }

    }

    $this->redirect('home/log');

  }

  public function checksms($smsid=0, $proccess=true){

    $error=null;
    $error_log=null;
    $exito=false;
    $isprep=false;


    $c=new Criteria();
    $c->addand(SmsPeer::ID, $smsid, Criteria::EQUAL);
    $sms = SmsPeer::doSelectOne($c);

    $data = strtolower($sms->getData());
    //$data = explode(" ", $data);

    if (strstr($data, '-')){
      $data = explode("-", $data);
      //echo "trae guion";
    }else{
      $data = explode(" ", $data);
      //echo "trae espacio";
    }

    //echo $sms->getData();
    //echo "<br/><br/>";
    //var_dump($data);

    //echo $data[0];

    $isdip=false;

    @$eleccion =$data[0];

    switch ($eleccion) {
      case 'nlgob':
        $estado ="tams";
        break;
      case 'nlpres':
        $estado ="tams";
        break;
      case 'nlpress':
        $estado ="tams";
        @$eleccion ="nlpres";
        break;
      case 'nlpre':
        $estado ="tams";
        @$eleccion ="nlpres";
        break;
      case 'nldip':
        $estado ="tams";
        @$eleccion ="nldip1";
        $isdip=true;
        $dipsec=1;
        break;
      case 'nldip1':
        $estado ="tams";
        $isdip=true;
        $dipsec=1;
        break;
      case 'nldip2':
        $estado ="tams";
        $isdip=true;
        $dipsec=2;
        break;
      case 'nldip3':
        $estado ="tams";
        $isdip=true;
        $dipsec=3;
        break;
      case 'agsgob':
        $estado ="ags";
        break;
      case 'agspres':
        $estado ="ags";
        break;
      case 'dgogob':
        $estado ="dgo";
        break;
      case 'dgopres':
        $estado ="dgo";
        break;
      default:
        $res["error"]=true;
        $res["error_log"]="Eleccion no encontrada: ".$eleccion;
        $res["exito"]=true;
        return $res;
      break;
    }

    //echo "Estado:".$estado;



    $pun=1;

    //detectar si es conteo rapido y brincarlo
    if ($data[$pun]=="cr"){
      $pun++;
      $isprep=true;
    }

    if ($data[$pun]=='sec' or $data[$pun]=='secc'){
      $pun++;
    }


    //echo "<br/>";
    //echo "eleccion:".$eleccion;

    

    @$seccion =$data[$pun];
    //VERIFICAR SECCION VALIDA
    if (!is_numeric($seccion)){
        $res["error"]=true;
        $res["error_log"]="seccion no valida NO ES NUMERICA: ".$seccion;
        $res["exito"]=true;
        return $res;
    }


    $pun++;

    //Search palabra
    $c=new Criteria(); 
    $c->addand(PalabraPeer::NAME, $estado, Criteria::EQUAL);
    $total = PalabraPeer::doCount($c);
    
    if ($total>=1){
      $palabra=PalabraPeer::doSelectOne($c);
    }else{
      $res["error"]=true;
      $res["error_log"]="palabra no encontrada: ".$estado;
      $res["exito"]=true;
      return $res;
    }

    //Search eleccion
    $c=new Criteria(); 
    $c->addand(EleccionPeer::PALABRA_ID, $palabra->getId(), Criteria::EQUAL);
    $c->addand(EleccionPeer::NAME, $eleccion, Criteria::EQUAL);
    $total = EleccionPeer::doCount($c);

    if ($total>=1){
      $eleccionr=EleccionPeer::doSelectOne($c);
    }else{
      $res["error"]=true;
      $res["error_log"]="eleccion no encontrada: ".$eleccion;
      $res["exito"]=true;
      return $res;
    }

    $sub_seccion="";
    if (!is_numeric($seccion)){
      $sub_seccion = substr($seccion, -1);
      $seccion = substr($seccion, 0, -1);
      //doble validacio para sub seccion
      if (!is_numeric($seccion)){
        $res["error"]=true;
        $res["error_log"]="seccion no valida: ".$seccion;
        $res["exito"]=true;
        return $res;
      }
    }

    //verificar la seccion con diputado
    if ($isdip and false){
      $c=new Criteria(); 
      $c->addand(SeccionPeer::PALABRA_ID, $palabra->getId(), Criteria::EQUAL);
      $c->addand(SeccionPeer::SECCION, $seccion, Criteria::EQUAL);
      $totals = SeccionPeer::doCount($c);
      if ($totals>=1){
        $seccionv=SeccionPeer::doSelectOne($c);
        if ($dipsec!=$seccionv->getDistritoLocalId()){
          $res["error"]=true;
          $res["error_log"]="seccion no coincide con el DL: $dipsec != ".$seccionv->getDistritoLocalId();
          $res["exito"]=true;
          return $res;
        }
      }
      //$dipsec
    }

    //verificar si existe un id en eleccion --para el caso de colima con ponderacion
    /*
    $c=new Criteria(); 
    $c->addand(SeccionPeer::PALABRA_ID, $palabra->getId(), Criteria::EQUAL);
    $c->addand(SeccionPeer::SECCION, $seccion, Criteria::EQUAL);
    $c->addand(SeccionPeer::SUB_SECCION, $sub_seccion, Criteria::EQUAL);
    $total = SeccionPeer::doCount($c);

    if ($total>=1){
      $seccionida=SeccionPeer::doSelectOne($c);
      $seccionid=$seccionida->getId();
    }else{
      $seccionid=null;
      $res["error"]=true;
      $res["error_log"]="seccion ID no encontrada: -".$seccion." ".$sub_seccion."-";
      $res["exito"]=true;
      return $res;
    }
    */

    /*
    echo "<br/>";
    echo "seccion:".$seccion;

    echo "<br/>";
    echo "pun:".$pun;

    echo "<br/>";
    echo "isprep:".$isprep;

    echo "<br/>";
    */

    //exit();

    //Search 
      $c=new Criteria(); 
      $c->addand(ResultadosPeer::ELECCION_ID, $eleccionr->getId(), Criteria::EQUAL);
      $c->addand(ResultadosPeer::SECCION, $seccion, Criteria::EQUAL);
      //$c->addand(ResultadosPeer::SUB_SECCION, $sub_seccion, Criteria::EQUAL);
      $total = ResultadosPeer::doCount($c);
      //echo "total: $total ---".$eleccionr->getId()."--".$seccion."--".$sub_seccion;
      //exit();
      if ($total>=1){
        $resultados=ResultadosPeer::doSelectOne($c);
        //hacer copia para la historia
        $resultadosh=new ResultadosHistoria();
        $resultadosh->setEleccionId($resultados->getEleccionId());
        $resultadosh->setEstado($resultados->getEstado());
        $resultadosh->setEleccion($resultados->getEleccion());
        $resultadosh->setSeccion($resultados->getSeccion());
        $resultadosh->setSubSeccion($resultados->getSubSeccion());
        $resultadosh->setSeccionId($resultados->getSeccionId());
        $resultadosh->setPonderacion($resultados->getPonderacion());

        $resultadosh->setPartido1($resultados->getPartido1());
        $resultadosh->setVotos1($resultados->getVotos1());
        $resultadosh->setPartido2($resultados->getPartido2());
        $resultadosh->setVotos2($resultados->getVotos2());
        $resultadosh->setPartido3($resultados->getPartido3());
        $resultadosh->setVotos3($resultados->getVotos3());
        $resultadosh->setPartido4($resultados->getPartido4());
        $resultadosh->setVotos4($resultados->getVotos4());
        $resultadosh->setPartido5($resultados->getPartido5());
        $resultadosh->setVotos5($resultados->getVotos5());
        $resultadosh->setPartido6($resultados->getPartido6());
        $resultadosh->setVotos6($resultados->getVotos6());
        $resultadosh->setPartido7($resultados->getPartido7());
        $resultadosh->setVotos7($resultados->getVotos7());
        $resultadosh->setPartido8($resultados->getPartido8());
        $resultadosh->setVotos8($resultados->getVotos8());
        $resultadosh->setPartido9($resultados->getPartido9());
        $resultadosh->setVotos9($resultados->getVotos9());
        $resultadosh->setPartido10($resultados->getPartido10());
        $resultadosh->setVotos10($resultados->getVotos10());
        $resultadosh->setPartido11($resultados->getPartido11());
        $resultadosh->setVotos11($resultados->getVotos11());
        $resultadosh->setPartido12($resultados->getPartido12());
        $resultadosh->setVotos12($resultados->getVotos12());
        $resultadosh->setPartido13($resultados->getPartido13());
        $resultadosh->setVotos13($resultados->getVotos13());
        $resultadosh->setPartido14($resultados->getPartido14());
        $resultadosh->setVotos14($resultados->getVotos14());
        $resultadosh->setPartido15($resultados->getPartido15());
        $resultadosh->setVotos15($resultados->getVotos15());
        $resultadosh->setCreatedAt($resultados->getCreatedAt());
        $resultadosh->setSmsId($resultados->getSmsId());

        if ($proccess){
          $resultadosh->save();
        }
      }else{
        $resultados=new Resultados();
        $resultados->setEstado($estado);
        $resultados->setEleccion($eleccion);
        $resultados->setEleccionId($eleccionr->getId());
        $resultados->setSeccion($seccion);
        $resultados->setSubSeccion($sub_seccion);
        if (@$seccionid){
          $resultados->setSeccionId($seccionid);
        } 
        $resultados->setCreatedAt(date('Y-m-d H:i:s', strtotime($sms->getCreatedAt())));
        $resultados->setSmsId($smsid);
        if ($proccess){
          $resultados->save();
        }
      }  

    //search seccion para la ponderacion
    $c=new Criteria(); 
    $c->addand(SeccionPeer::PALABRA_ID, $palabra->getId(), Criteria::EQUAL);
    $c->addand(SeccionPeer::SECCION, $seccion, Criteria::EQUAL);
    $c->addand(SeccionPeer::SUB_SECCION, $sub_seccion, Criteria::EQUAL);
    $total = SeccionPeer::doCount($c);
    if ($total>=1){
      $seccionv=SeccionPeer::doSelectOne($c);
      $resultados->setPonderacion($seccionv->getPonderacion());
    }  

    $error_log="";
    $cg=1;
    do {
      $cg++;
      //Search eleccion
      $c=new Criteria(); 
      $c->addand(PartidoPeer::NAME, @$data[$pun], Criteria::EQUAL);
      $total = PartidoPeer::doCount($c);

      //Si es un partido
      if ($total>=1){
        $partido=PartidoPeer::doSelectOne($c);
        //verificar si el dato subsecuente es numerico
        //var_dump($data);
        //exit();

        if (is_numeric($data[$pun+1])){
          switch ($partido->getId()) {
              case 1:
                  $resultados->setPartido1($data[$pun]);
                  $resultados->setVotos1($data[$pun+1]);
                break;
              case 2:
                  $resultados->setPartido2($data[$pun]);
                  $resultados->setVotos2($data[$pun+1]);
                break;
              case 3:
                  $resultados->setPartido3($data[$pun]);
                  $resultados->setVotos3($data[$pun+1]);
                break;
              case 4:
                  $resultados->setPartido4($data[$pun]);
                  $resultados->setVotos4($data[$pun+1]);
                break;
              case 5:
                  $resultados->setPartido5($data[$pun]);
                  $resultados->setVotos5($data[$pun+1]);
                break;
              case 6:
                  $resultados->setPartido6($data[$pun]);
                  $resultados->setVotos6($data[$pun+1]);
                break;
              case 7:
                  $resultados->setPartido7($data[$pun]);
                  $resultados->setVotos7($data[$pun+1]);
                break;
              case 8:
                  $resultados->setPartido8($data[$pun]);
                  $resultados->setVotos8($data[$pun+1]);
                break;
              case 9:
                  $resultados->setPartido9($data[$pun]);
                  $resultados->setVotos9($data[$pun+1]);
              break;
              case 10:
                  $resultados->setPartido10($data[$pun]);
                  $resultados->setVotos10($data[$pun+1]);
              break;
              case 11:
                  $resultados->setPartido11($data[$pun]);
                  $resultados->setVotos11($data[$pun+1]);
              break;
              case 12:
                  $resultados->setPartido12($data[$pun]);
                  $resultados->setVotos12($data[$pun+1]);
              break;
              case 13:
                  $resultados->setPartido13($data[$pun]);
                  $resultados->setVotos13($data[$pun+1]);
              break;
              case 14:
                  $resultados->setPartido14($data[$pun]);
                  $resultados->setVotos14($data[$pun+1]);
              break;
              case 15:
                  $resultados->setPartido15($data[$pun]);
                  $resultados->setVotos15($data[$pun+1]);
              break;
          }
                    
          $pun= $pun+1;
        }else{
          //El datos subsecuente al partido no es numerico
        }
      }else{
        //No se encontro el partido 
        if (!is_numeric(@$data[$pun]) and @$data[$pun]!=""){
          $error_log.=@$data[$pun]." no encontrado | ";
          $sms->setChecked(2);
        }
      }

      $pun++;
    } while ($cg <= 14);



    //$resultados->setCreatedAt(date('Y-m-d H:i:s'));
    $resultados->setEleccionId($eleccionr->getId());
    $resultados->setCreatedAt(date('Y-m-d H:i:s', strtotime($sms->getCreatedAt())));
    $resultados->setSmsId($smsid);

    $sms->setErrorLog($error_log);
    $sms->setError(false);


    $res["error"]=false;
    $res["error_log"]=$error_log;
    $res["exito"]=true;

    

    if ($proccess){
      $c=new Criteria();
      $config = ConfigPeer::doSelectOne($c);

      if ($config->getCanProcess()){
        $sms->setProcessed(1);
        $sms->setResultados($resultados);
        $sms->setChecked(1);
        //AQUI TOMAR ESTE VALOR DEL DE CONFIG
        if ($isprep){
          $resultados->setPrep($isprep);
        }else{
          $resultados->setPrep($config->getIsPrep());
        }
        $resultados->save();
        //echo $resultados->getId()."--".$resultados->getPartido9().$resultados->getVotos9();
        //exit();
      }

    }else{
      $sms->setChecked(1);
    }

    $sms->save();

    //var_dump($res);
    //exit(); 
    
    return $res;


  }



}


  function enviarsms($host, $port, $username, $password, $phoneNoRecip, $msgText) {
    
    $sms=new SmsSender();
    $sms->setModem($host);
    //$sms->setFrom('localhost');
    $sms->setTo($phoneNoRecip);
    $sms->setSms($msgText);
    $sms->setStatus(false);
    $sms->save();

    $fp = @fsockopen($host, $port, $errno, $errstr, 1);
    if (!$fp) {
      return $errstr;
    }

    @fwrite($fp, "GET /?Username=" .$username . "&Password=" . $password . "&Phone=" . urlencode('+52'.$phoneNoRecip) . "&Text=" . rawurlencode($msgText) . " HTTP/1.0\n");
    if ($username != "") {
       $auth = $username . ":" . $password;
       //echo "auth: $auth\n";
       $auth = @base64_encode($auth);
       //echo "auth: $auth\n";
       @fwrite($fp, "Authorization: Basic " . $auth . "\n");
    }
    @fwrite($fp, "\n");
    $res = "";
    while(!@feof($fp)) {
      $res .= @fread($fp,1);
    }
    @fclose($fp);

    var_dump($res);
    exit();

    $sms->setStatus(false);
    $sms->save();


    return $res;
  }

  function sksort(&$array, $subkey="id", $sort_ascending=false) {

    if (count($array))
        $temp_array[key($array)] = array_shift($array);

    foreach($array as $key => $val){
        $offset = 0;
        $found = false;
        foreach($temp_array as $tmp_key => $tmp_val)
        {
            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
            {
                $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
                                            array($key => $val),
                                            array_slice($temp_array,$offset)
                                          );
                $found = true;
            }
            $offset++;
        }
        if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
    }

    if ($sort_ascending) $array = array_reverse($temp_array);

    else $array = $temp_array;
}
