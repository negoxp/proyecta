<H1>Elecciones <strong><?php echo  $eleccion->getPalabra()->getName();?> <?php echo $eleccion->getName();?><strong></H1>

<table class="table table-condensed table-hover datatable">
    <thead>
    <tr>
        <th>Seccion</th>
    	<th>SMS- Procesados</th>
    	<th>Ultimo SMS -procesado</th>
    	<th>Encuestador</th>
    	<th>Cel Encuestador</th>
    </tr>
	</thead>
	
	<?php
    $i=1;
    foreach ($secciones as $seccion):
    ?>
	<tr>
        <td><?php echo $seccion->getSeccion(); ?></td>
    	<td></td>
    	<td>
    		<?php
    		$c=new Criteria(); 
    		$c->addand(ResultadosPeer::ELECCION_ID, $eleccion->getId() , Criteria::EQUAL);
    		$c->addand(ResultadosPeer::SECCION_ID, $seccion->getId() , Criteria::EQUAL); 
            //$c->addand(ResultadosPeer::SECCION_ID, $seccion->getSeccion() , Criteria::EQUAL);
            $c->addand(ResultadosPeer::PREP, TRUE, Criteria::EQUAL);
    		$c->addDescendingOrderByColumn(ResultadosPeer::ID);
    		$resultados = ResultadosPeer::doSelectOne($c);
    		if ($resultados){
    			echo date("h:i:s A",strtotime($resultados->getCreatedAt()));
    		}
    		?>
    	</td>
    	<td><?php echo $seccion->getEncuestador(); ?></td>
    	<td><?php echo $seccion->getCelEncuestador(); ?></td>
    </tr>
    
    <?php 
    $i++;
    endforeach; ?>
</table>


<script type="text/javascript">
$(document).ready( function () {
    $('.datatable').dataTable({
      "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                {
                    "sExtends": "xls",
                    "sTitle": "XLS"
                },
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sTitle": "PDF"
                }
            ]
        },
      aLengthMenu: [
        [10, 25, 100, -1],
        [10, 25, 100, "Todos"]
      ],
      iDisplayLength: -1,
    });



} );
</script>