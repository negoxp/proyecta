<h3>Resultado Detalle ID: <?php echo $resultado->getId(); ?></h3>

<table class="table">
<tr>
	<th>Id</th>
	<th>Estado</th>
	<th>Eleccion</th>
	<th>Seccion</th>
	<th>PRI</th>
	<th>PAN</th>
	<th>PRD</th>
	<th>MC</th>
	<th>PES</th>
	<th>MOR</th>
	<th>NU</th>
	<th>NC</th>
	<th>Fecha</th>
</tr>
<tr>
	<td><?php echo $resultado->getId(); ?></td>
	<td><?php echo $resultado->getEstado(); ?></td>
	<td><?php echo $resultado->getEleccion(); ?></td>
	<td><?php echo $resultado->getSeccion(); ?></td>
	<td><?php echo $resultado->getVotos1(); ?></td>
	<td><?php echo $resultado->getVotos2(); ?></td>
	<td><?php echo $resultado->getVotos3(); ?></td>
	<td><?php echo $resultado->getVotos4(); ?></td>
	<td><?php echo $resultado->getVotos5(); ?></td>
	<td><?php echo $resultado->getVotos6(); ?></td>
	<td><?php echo $resultado->getVotos7(); ?></td>
	<td><?php echo $resultado->getVotos8(); ?></td>
	<td><?php echo $resultado->getCreatedAt(); ?></td>
</tr>
</table>


<table class="table">
<tr>
	<th>SMS Original</th>
	<td>
<code class="language-html" data-lang="html">
    <?php echo $resultado->getSms()->getDataOriginal(); ?>
</code>
	</td>
</tr>
<tr>
	<th>Celular</th>
	<td><?php echo $resultado->getSms()->getSender(); ?></td>
</tr>
<tr>
	<th>Modem</th>
	<td><?php echo $resultado->getSms()->getModem(); ?></td>
</tr>
<tr>
	<th>Data</th>
	<td><?php echo $resultado->getSms()->getData(); ?></td>
</tr>
<tr>
	<th>Fecha</th>
	<td><?php echo $resultado->getSms()->getCreatedAt(); ?></td>
</tr>
<?php if (is_numeric($resultado->getSeccionId())){

$seccion = SeccionPeer::retrieveByPk($resultado->getSeccionId());
?>

<tr>
	<th>Encuestador</th>
	<td><?php echo $seccion->getEncuestador(); ?></td>
</tr>

<tr>
	<th>Celular</th>
	<td><?php echo $seccion->getCelEncuestador(); ?></td>
</tr>

<?php }?>
</table>

<h3>Historico Resultados</h3>

<table class="table">
<tr>
	<th>Id</th>
	<th>Estado</th>
	<th>Eleccion</th>
	<th>Seccion</th>
	<th>PRI</th>
	<th>PAN</th>
	<th>PRD</th>
	<th>MC</th>
	<th>PES</th>
	<th>MOR</th>
	<th>NU</th>
	<th>NC</th>
	<th>Fecha</th>
</tr>
<?php

$c=new Criteria(); 
$c->addand(ResultadosHistoriaPeer::ELECCION_ID, $resultado->getEleccionId(), Criteria::EQUAL);
$c->addand(ResultadosHistoriaPeer::SECCION, $resultado->getSeccion(), Criteria::EQUAL);
$c->addGroupByColumn(ResultadosHistoriaPeer::SMS_ID);
$c->addDescendingOrderByColumn(ResultadosHistoriaPeer::CREATED_AT); 
$resultados = ResultadosHistoriaPeer::doSelect($c);

foreach ($resultados as $resultado){


?>
<tr>
	<td><?php echo $resultado->getId(); ?></td>
	<td><?php echo $resultado->getEstado(); ?></td>
	<td><?php echo $resultado->getEleccion(); ?></td>
	<td><?php echo $resultado->getSeccion(); ?></td>
	<td><?php echo $resultado->getVotos1(); ?></td>
	<td><?php echo $resultado->getVotos2(); ?></td>
	<td><?php echo $resultado->getVotos3(); ?></td>
	<td><?php echo $resultado->getVotos4(); ?></td>
	<td><?php echo $resultado->getVotos5(); ?></td>
	<td><?php echo $resultado->getVotos6(); ?></td>
	<td><?php echo $resultado->getVotos7(); ?></td>
	<td><?php echo $resultado->getVotos8(); ?></td>
	<td><?php echo date("h:i:s A",strtotime($resultado->getCreatedAt())); ?></td>
</tr>
<tr>
	<td colspan="17">
		SMS ORIGINAL: 
		<code class="language-html" data-lang="html">
    <?php echo $resultado->getSms()->getDataOriginal(); ?>
</code>
	</td>
</tr>

<?php
}
?>
</table>








