<H1>Logs</H1>

<form action="<?php echo url_for('home/multiprocess') ?>" method="post" id="formsms" >
    <!--
<a class="btn btn-default" href="<?php echo url_for("home/checkallsms") ?>" >Check all sms </a>
<a class="btn btn-default" href="<?php echo url_for("home/fixsms") ?>" >Fix sms </a>
<a class="btn btn-default" href="<?php echo url_for("home/processallsms") ?>" >Process all sms </a>
-->
<input type="submit" value="Check" class="btn btn-default smit" name="check"/>
<input type="submit" value="Fix" class="btn btn-default smit" name="fix"/>
<?php if ($canprocess) { ?>
<input type="submit" value="Process" class="btn btn-default smit" name="process"/>
<?php } ?>
<br/><br/>

<table class="table">
    <thead>
    <tr>
        <th class="bg-gray"><input type="checkbox" id="select_all" />Id</th>
        <th class="bg-blue">Cel. Origen</th>
        <th class="bg-blue" style="width:40%;">Mensaje</th>
        <th class="bg-blue"><i class="fa fa-cog"></i></th>
        <th class="bg-blue"><i class="fa fa-exclamation-triangle"></i> Error</th>
        <th class="bg-blue"><i class="fa fa-eye"></i>
        <th class="bg-blue">Accion</th></th>
        <th class="bg-blue">Created at</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $tt=0;
    foreach ($smss as $sms):
    $tt++;
    ?>
    <?php
            switch ($sms->getChecked()) {
              case 1:
                  $class="text-primary";
                  $classtr="info";
              break;
              case 2:
                  $class="text-danger";
                  $classtr="danger";
              break;
              default:
                 $class="text-default";
                 $classtr="";
              break;
            }
            if ($sms->getManualEdited()){
                $classtr="success";
            }
            ?>
    <tr class="<?php echo $classtr; ?>">
        <td><input type="checkbox" name="asms[]" value="<?php echo $sms->getId(); ?>" /><?php echo $sms->getId(); ?></td>
        <td><?php echo $sms->getSender(); ?></td>
        <td style="width:40%;">
        <div style="max-width:350px;">
            <?php echo $sms->getData(); ?>
        </div>
        </td>
        <td>
            <?php if ($sms->getProcessed()){ ?>
            <span class="label label-success"><i class="fa fa-check"></i> </span>
            <?php }else{?>
            <span class="label label-info"><i class="fa fa-minus"></i> </span>
            <?php } ?>
        </td>
        <td>
            <?php if ($sms->getError()){ ?>
            <span class="label label-danger" data-toggle="tooltip" data-placement="bottom" title="<?php echo $sms->getErrorLog(); ?>"><i class="fa fa-exclamation-triangle"></i> Error</span>
            <?php } ?>
        </td>
        <td>
            <span class="btn" data-toggle="tooltip" data-placement="bottom" title="<?php echo $sms->getErrorLog(); ?>">
            <i class="fa fa-circle <?php echo $class; ?>"></i>
            </span>
        </td>
        <td>
            <?php if ($canprocess) { ?>
            <a  class="btn btn-primary btn-sm" href="<?php echo url_for("home/psms?smsid=".$sms->getId()) ?>"><i class="fa fa-cog"></i></a> 
            <?php } ?>
            <a  class="btn btn-default btn-sm" href="<?php echo url_for('sms/edit?id='.$sms->getId()) ?>"><i class="fa fa-pencil"></i></a>
            <a class="btn btn-warning btn-sm"  href="<?php echo url_for("home/hablar?smsid=".$sms->getId()) ?>"><i class="fa fa-phone"></i></a>
            <a class="btn btn-info btn-sm"  href="<?php echo url_for("home/sms?smsid=".$sms->getId()) ?>"><i class="fa fa-envelope"></i></a>
        </td>
        <td><?php echo date("h:i:s A",strtotime($sms->getCreatedAt())); ?>
        <a class="btn btn-danger pull-right btn-sm" onclick="return confirm('Are you sure?')" href="<?php echo url_for("home/logicaldelete?smsid=".$sms->getId()) ?>"><i class="fa fa-times"></i></a>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</form>


<?php echo "Total: ".$tt; ?>

<script type="text/javascript">
$(document).ready( function () {
    $('.datatable').dataTable({
      "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                {
                    "sExtends": "xls",
                    "sTitle": "XLS"
                },
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sTitle": "PDF"
                }
            ]
        },
      aLengthMenu: [
        [10, 25, 100, -1],
        [10, 25, 100, "Todos"]
      ],
      iDisplayLength: 25,
    });

    $('#select_all').change(function() {
        var c = this.checked;
        $(':checkbox').prop('checked',c);
    });

    /*
    $( ".smit" ).click(function() {
        $( ".smit" ).attr("value", false);
        $(this).attr("value", "si");
        $( "#formsms" ).submit();
    });
*/

    $( "#formsms" ).submit(function () {
        // Get the submit button element
        var btn = $(this).find("input[type=submit]:focus" );
        btn.attr("value", true);
    });


} );
</script>