


                <H1>Log</H1>

                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th class="bg-gray">Id</th>
                        <th class="bg-blue">Modem</th>
                        <th class="bg-blue">Cel. Origen</th>
                        <th class="bg-blue">Mensaje</th>
                        <th class="bg-blue">Procesado</th>
                        <th class="bg-blue">Accion</th>
                        <th class="bg-blue">Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($smss as $sms):
                    ?>
                    <tr>
                        <td class="bg-gray"><?php echo $sms->getId(); ?></td>
                        <td class="bg-gray"><?php echo $sms->getModem(); ?></td>
                        <td class="bg-gray"><?php echo $sms->getSender(); ?></td>
                        <td class="bg-gray"><?php echo $sms->getData(); ?></td>
                        <td class="bg-gray"><?php echo $sms->getProcessed(); ?></td>
                        <td class="bg-gray">
                            <a href="<?php echo url_for("home/psms?smsid=".$sms->getId()) ?>">Procesar </a>
                        </td>
                        <td class="bg-gray"><?php echo $sms->getCreatedAt(); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>


                <H1>Resultados</H1>
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th class="bg-gray">Id</th>
                        <th class="bg-blue">Estado</th>
                        <th class="bg-blue">Eleccion</th>
                        <th class="bg-blue">Seccion</th>
                        <th class="bg-blue">Partido</th>
                        <th class="bg-blue">Votos</th>
                        <th class="bg-blue">Partido</th>
                        <th class="bg-blue">Votos</th>
                        <th class="bg-blue">Partido</th>
                        <th class="bg-blue">Votos</th>
                        <th class="bg-blue">Partido</th>
                        <th class="bg-blue">Votos</th>
                        <th class="bg-blue">Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($resultados as $resultado):
                    ?>
                    <tr>
                        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getEleccion(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getSeccion(); ?></td>

                        <td class="bg-gray"><?php echo $resultado->getPartido1(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getVotos1(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getPartido2(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getVotos2(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getPartido3(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getVotos3(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getPartido4(); ?></td>
                        <td class="bg-gray"><?php echo $resultado->getVotos4(); ?></td>



                        <td class="bg-gray"><?php echo $resultado->getCreatedAt(); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>




