 <H1>Elecciones</H1>
<table class="table table-condensed table-hover">
    <thead>
    <tr>
        <th class="bg-gray">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
        <th class="bg-aqua">Secciones</th>

    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($elecciones as $eleccion):

   	$c=new Criteria(); 
    $c->addand(ResultadosPeer::ELECCION_ID, $eleccion->getId(), Criteria::EQUAL);
    $resultados = ResultadosPeer::doSelect($c);

    $total = ResultadosPeer::doCount($c);
    if ($total>=1){

        $pv1=0; 
        $pv2=0; 
        $pv3=0; 
        $pv4=0; 
        $pv5=0; 
        $pv6=0; 
        $pv7=0; 
        $pv8=0; 
        $pv9=0; 
        $pv10=0; 
        $pv11=0; 
        $pv12=0; 
  

    	foreach ($resultados as $resultado){
    		@$pv1+=$resultado->getVotos1();	
    		@$pv2+=$resultado->getVotos2();	
    		@$pv3+=$resultado->getVotos3();	
    		@$pv4+=$resultado->getVotos4();	
    		@$pv5+=$resultado->getVotos5();	
    		@$pv6+=$resultado->getVotos6();	
    		@$pv7+=$resultado->getVotos7();	
    		@$pv8+=$resultado->getVotos8();	
    		@$pv9+=$resultado->getVotos8();	
    		@$pv10+=$resultado->getVotos10();	
    	}


    ?>
    <tr>
        <td class="bg-gray"><?php echo $eleccion->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray">
            <a href="<?php if ($eleccion->getId()==1) { echo url_for("home/deleccion?eid=".$eleccion->getId()); } else{ echo url_for("home/deleccion?eid=".$eleccion->getId()); } ?>"><i class="fa fa-cog"></i>
            <?php echo $resultado->getEleccion(); ?>
            </a>
            <br/>
            <a href="<?php if ($eleccion->getId()==1) { echo url_for("home/deleccion?eid=".$eleccion->getId()."&nc=1"); } else{ echo url_for("home/deleccion?eid=".$eleccion->getId()); } ?>"><i class="fa fa-cog"></i>
            <?php echo $resultado->getEleccion(); ?>-NC
            </a> 
            <!-- 
            <br/>
            <a href="<?php if ($eleccion->getId()==1) { echo url_for("home/deleccioncol?pv=1&eid=".$eleccion->getId()); } else{ echo url_for("home/deleccion?eid=".$eleccion->getId()); } ?>"><i class="fa fa-cog"></i>
            <?php echo $resultado->getEleccion(); ?> PV
            </a> 
            <br/>
            <a href="<?php if ($eleccion->getId()==1) { echo url_for("home/deleccioncol?eid=".$eleccion->getId()."&nc=1&pv=1"); } else{ echo url_for("home/deleccion?eid=".$eleccion->getId()); } ?>"><i class="fa fa-cog"></i>
            <?php echo $resultado->getEleccion(); ?>-NC-PV
            </a> 
            -->
            <br/>
            <a href="<?php if ($eleccion->getId()==1) { echo url_for("home/deleccioncol?eid=".$eleccion->getId()."&prep=1"); } else{ echo url_for("home/deleccion?eid=".$eleccion->getId()); } ?>"><i class="fa fa-cog"></i>
            <?php echo $resultado->getEleccion(); ?>-PREP
            </a>  
        </td> 
        <td class="bg-gray">
            <a href="<?php echo url_for("home/seccionessms?eid=".$eleccion->getId()); ?>">
            <i class="fa fa-cog"></i>
            Secciones log
            </a>  
        </td> 
        <!--
        <td class="bg-gray"><?php echo $resultado->getPartido1(); ?></td>
        <td class="bg-gray"><?php echo $pv1 ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido2(); ?></td>
        <td class="bg-gray"><?php echo $pv2; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido3(); ?></td>
        <td class="bg-gray"><?php echo $pv3; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido4(); ?></td>
        <td class="bg-gray"><?php echo $pv4; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido5(); ?></td>
        <td class="bg-gray"><?php echo $pv5; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido6(); ?></td>
        <td class="bg-gray"><?php echo $pv6; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido7(); ?></td>
        <td class="bg-gray"><?php echo $pv7; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido8(); ?></td>
        <td class="bg-gray"><?php echo $pv8; ?></td>
        <td class="bg-gray"><?php echo $resultado->getPartido10(); ?></td>
        <td class="bg-gray"><?php echo $pv10; ?></td>
    -->
    </tr>
    <?php } else {?>
        <tr>
        <td class="bg-gray"><?php echo $eleccion->getId(); ?></td>
        <td class="bg-gray"><?php echo $eleccion->getPalabra()->getName(); ?></td>
        <td class="bg-gray"><?php echo $eleccion->getName(); ?></td>

        <td class="bg-gray">
            <a href="<?php echo url_for("home/seccionessms?eid=1"); ?>">
            <i class="fa fa-cog"></i>
            Secciones log
            </a>  
        </td>
        <!--
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>
        <td class="bg-gray"></td>

        -->

    </tr>
    <?php } ?>
    <?php endforeach; ?>
    </tbody>
</table>

