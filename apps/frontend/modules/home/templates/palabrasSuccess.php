<H1>Estados</H1>
<table class="table table-condensed table-hover">
    <thead>
    <tr>
        <th class="bg-gray">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Sms</th>
        <th class="bg-blue">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($resultados as $resultado):
    ?>
    <tr>
        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray">#</td>

        <td class="bg-gray">
        <a href="<?php echo url_for("home/eleccion?eid=".$resultado->getId()) ?>"><button type="button" class="btn btn-info">Ver</button></a>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>