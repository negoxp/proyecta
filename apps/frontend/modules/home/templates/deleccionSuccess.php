    <?php

sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));  
$objPHPExcel = new PHPExcel();  

$inputFileName = sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR.'plantilla.xlsx';

//  Read your Excel workbook
  try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
  } catch (Exception $e) {
      die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
      . '": ' . $e->getMessage());
  }


//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$fila=1; 

    foreach ($resultados as $resultado){
        if ($resultado->getPartido1()!=""){ @$pn1=$resultado->getPartido1(); }
        if ($resultado->getPartido2()!=""){ @$pn2=$resultado->getPartido2(); }
        if ($resultado->getPartido3()!=""){ @$pn3=$resultado->getPartido3(); }
        if ($resultado->getPartido4()!=""){ @$pn4=$resultado->getPartido4(); }
        if ($resultado->getPartido5()!=""){ @$pn5=$resultado->getPartido5(); }
        if ($resultado->getPartido6()!=""){ @$pn6=$resultado->getPartido6(); }
        if ($resultado->getPartido7()!=""){ @$pn7=$resultado->getPartido7(); }
        if ($resultado->getPartido8()!=""){ @$pn8=$resultado->getPartido8(); }
        if ($resultado->getPartido9()!=""){ @$pn9=$resultado->getPartido9(); }
        if ($resultado->getPartido10()!=""){ @$pn10=$resultado->getPartido10(); }
        if ($resultado->getPartido11()!=""){ @$pn11=$resultado->getPartido11(); }
        if ($resultado->getPartido12()!=""){ @$pn12=$resultado->getPartido12(); }
        if ($resultado->getPartido13()!=""){ @$pn13=$resultado->getPartido13(); }
        if ($resultado->getPartido14()!=""){ @$pn14=$resultado->getPartido14(); }
        if ($resultado->getPartido15()!=""){ @$pn15=$resultado->getPartido15(); }

    }

$objPHPExcel->getActiveSheet()->SetCellValue('A1', $resultado->getEstado()." ".$resultado->getEleccion() );

?> 

<H1>Elecciones <strong><?php echo  $resultado->getEstado();?> <?php echo $resultado->getEleccion();?><strong></H1>
<H3>Votos</H3>
<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <!--
        <th>Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
        -->
        <th class="bg-blue">Seccion</th>
        <?php if (@$pn1!="") { ?><th class="bg-aqua"><?php echo @$pn1; ?></th><?php } ?>
        <?php if (@$pn2!="") { ?><th class="bg-aqua"><?php echo @$pn2; ?></th><?php } ?>
        <?php if (@$pn3!="") { ?><th class="bg-aqua"><?php echo @$pn3; ?></th><?php } ?>
        <?php if (@$pn4!="") { ?><th class="bg-aqua"><?php echo @$pn4; ?></th><?php } ?>
        <?php if (@$pn5!="") { ?><th class="bg-aqua"><?php echo @$pn5; ?></th><?php } ?>
        <?php if (@$pn6!="") { ?><th class="bg-aqua"><?php echo @$pn6; ?></th><?php } ?>
        <?php if (@$pn7!="") { ?><th class="bg-aqua"><?php echo @$pn7; ?></th><?php } ?>
        <?php if (@$pn8!="") { ?><th class="bg-aqua"><?php echo @$pn8; ?></th><?php } ?>
        <?php if (@$pn9!="") { ?><th class="bg-aqua"><?php echo @$pn9; ?></th><?php } ?>
        <?php if (@$pn10!="") { ?><th class="bg-aqua"><?php echo @$pn10; ?></th><?php } ?>
        <?php if (@$pn11!="") { ?><th class="bg-aqua"><?php echo @$pn11; ?></th><?php } ?>
        <?php if (@$pn12!="") { ?><th class="bg-aqua"><?php echo @$pn12; ?></th><?php } ?>
        <?php if (@$pn13!="") { ?><th class="bg-aqua"><?php echo @$pn13; ?></th><?php } ?>
        <?php if (@$pn14!="") { ?><th class="bg-aqua"><?php echo @$pn14; ?></th><?php } ?>
        <?php if (@$pn15!="") { ?><th class="bg-aqua"><?php echo @$pn15; ?></th><?php } ?>
        <th class="bg-aqua">Total</th>
        <th class="bg-aqua"></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $fila=7;
    foreach ($resultados as $resultado):

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, $resultado->getSeccion());

    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, $resultado->getVotos1());
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila, $resultado->getVotos2());
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $resultado->getVotos3());
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $resultado->getVotos4());
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $resultado->getVotos5());
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $resultado->getVotos6());
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $resultado->getVotos7());
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $resultado->getVotos8());
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $resultado->getVotos9());
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $resultado->getVotos10());
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$fila, $resultado->getVotos11());
    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$fila, $resultado->getVotos12());
    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$fila, $resultado->getVotos13());
    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$fila, $resultado->getVotos14());
    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$fila, $resultado->getVotos15());

    $fila++;

    @$pv1+=$resultado->getVotos1(); 
    @$pv2+=$resultado->getVotos2(); 
    @$pv3+=$resultado->getVotos3(); 
    @$pv4+=$resultado->getVotos4(); 
    @$pv5+=$resultado->getVotos5(); 
    @$pv6+=$resultado->getVotos6(); 
    @$pv7+=$resultado->getVotos7(); 
    @$pv8+=$resultado->getVotos8(); 
    @$pv9+=$resultado->getVotos9(); 
    @$pv10+=$resultado->getVotos10();
    @$pv11+=$resultado->getVotos11();
    @$pv12+=$resultado->getVotos12();
    @$pv13+=$resultado->getVotos13();
    @$pv14+=$resultado->getVotos14();
    @$pv15+=$resultado->getVotos15();

    @$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9()+$resultado->getVotos10()+$resultado->getVotos11()+$resultado->getVotos12()+$resultado->getVotos13()+$resultado->getVotos14()+$resultado->getVotos15(); 
    @$tvg=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+$pv9+$pv10+$pv11+$pv12+$pv13+$pv14+$pv15;
        

?>
    <tr>
        <!--
        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEleccion(); ?></td>
        -->
        <td class="bg-gray"><?php echo $resultado->getSeccion(); ?></td>
        <?php if (@$pn1!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos1(); ?></td><?php } ?>
        <?php if (@$pn2!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos2(); ?></td><?php } ?>
        <?php if (@$pn3!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos3(); ?></td><?php } ?>
        <?php if (@$pn4!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos4(); ?></td><?php } ?>
        <?php if (@$pn5!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos5(); ?></td><?php } ?>
        <?php if (@$pn6!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos6(); ?></td><?php } ?>
        <?php if (@$pn7!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos7(); ?></td><?php } ?>
        <?php if (@$pn8!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos8(); ?></td><?php } ?>
        <?php if (@$pn9!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos9(); ?></td><?php } ?>
        <?php if (@$pn10!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos10(); ?></td><?php } ?>
        <?php if (@$pn11!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos11(); ?></td><?php } ?>
        <?php if (@$pn12!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos12(); ?></td><?php } ?>
        <?php if (@$pn13!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos13(); ?></td><?php } ?>
        <?php if (@$pn14!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos14(); ?></td><?php } ?>
        <?php if (@$pn15!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos15(); ?></td><?php } ?>
        <td class="bg-gray"><?php echo $tv; ?></td>
        <td class="bg-gray"><a href="<?php echo url_for("home/resultado_history?rid=".$resultado->getId()) ?>"><i class="fa fa-eye"></i></a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <!--
        <th class="bg-blue">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
        -->
        <th class="bg-blue">Seccion</th>
        <?php if (@$pn1!="") { ?><th class="bg-aqua"><?php echo @$pn1; ?></th><?php } ?>
        <?php if (@$pn2!="") { ?><th class="bg-aqua"><?php echo @$pn2; ?></th><?php } ?>
        <?php if (@$pn3!="") { ?><th class="bg-aqua"><?php echo @$pn3; ?></th><?php } ?>
        <?php if (@$pn4!="") { ?><th class="bg-aqua"><?php echo @$pn4; ?></th><?php } ?>
        <?php if (@$pn5!="") { ?><th class="bg-aqua"><?php echo @$pn5; ?></th><?php } ?>
        <?php if (@$pn6!="") { ?><th class="bg-aqua"><?php echo @$pn6; ?></th><?php } ?>
        <?php if (@$pn7!="") { ?><th class="bg-aqua"><?php echo @$pn7; ?></th><?php } ?>
        <?php if (@$pn8!="") { ?><th class="bg-aqua"><?php echo @$pn8; ?></th><?php } ?>
        <?php if (@$pn9!="") { ?><th class="bg-aqua"><?php echo @$pn9; ?></th><?php } ?>
        <?php if (@$pn10!="") { ?><th class="bg-aqua"><?php echo @$pn10; ?></th><?php } ?>
        <?php if (@$pn11!="") { ?><th class="bg-aqua"><?php echo @$pn11; ?></th><?php } ?>
        <?php if (@$pn12!="") { ?><th class="bg-aqua"><?php echo @$pn12; ?></th><?php } ?>
        <?php if (@$pn13!="") { ?><th class="bg-aqua"><?php echo @$pn13; ?></th><?php } ?>
        <?php if (@$pn14!="") { ?><th class="bg-aqua"><?php echo @$pn14; ?></th><?php } ?>
        <?php if (@$pn15!="") { ?><th class="bg-aqua"><?php echo @$pn15; ?></th><?php } ?>
        <th class="bg-aqua">Total</th>
        <th class="bg-aqua"></th>
    </tr>
    <tr>
        <th class="bg-blue" colspan="1">Total</th>
        <?php if (@$pn1!="") { ?><th class="bg-aqua"><?php echo $pv1; ?></th><?php } ?>
        <?php if (@$pn2!="") { ?><th class="bg-aqua"><?php echo $pv2; ?></th><?php } ?>
        <?php if (@$pn3!="") { ?><th class="bg-aqua"><?php echo $pv3; ?></th><?php } ?>
        <?php if (@$pn4!="") { ?><th class="bg-aqua"><?php echo $pv4; ?></th><?php } ?>
        <?php if (@$pn5!="") { ?><th class="bg-aqua"><?php echo $pv5; ?></th><?php } ?>
        <?php if (@$pn6!="") { ?><th class="bg-aqua"><?php echo $pv6; ?></th><?php } ?>
        <?php if (@$pn7!="") { ?><th class="bg-aqua"><?php echo $pv7; ?></th><?php } ?>
        <?php if (@$pn8!="") { ?><th class="bg-aqua"><?php echo $pv8; ?></th><?php } ?>
        <?php if (@$pn9!="") { ?><th class="bg-aqua"><?php echo $pv9; ?></th><?php } ?>
        <?php if (@$pn10!="") { ?><th class="bg-aqua"><?php echo $pv10; ?></th><?php } ?>
        <?php if (@$pn11!="") { ?><th class="bg-aqua"><?php echo $pv11; ?></th><?php } ?>
        <?php if (@$pn12!="") { ?><th class="bg-aqua"><?php echo $pv12; ?></th><?php } ?>
        <?php if (@$pn13!="") { ?><th class="bg-aqua"><?php echo $pv13; ?></th><?php } ?>
        <?php if (@$pn14!="") { ?><th class="bg-aqua"><?php echo $pv14; ?></th><?php } ?>
        <?php if (@$pn15!="") { ?><th class="bg-aqua"><?php echo $pv15; ?></th><?php } ?>
        <td class="bg-aqua"><?php echo $tvg; ?></td>
        <th class="bg-aqua"></th>
    </tr>
    </tfoot>
</table>


<H3>% Votos</H3>
<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <!--
        <th class="bg-blue">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
    -->
        <th class="bg-blue">Seccion</th>
        <?php if (@$pn1!="") { ?><th class="bg-aqua"><?php echo @$pn1; ?></th><?php } ?>
        <?php if (@$pn2!="") { ?><th class="bg-aqua"><?php echo @$pn2; ?></th><?php } ?>
        <?php if (@$pn3!="") { ?><th class="bg-aqua"><?php echo @$pn3; ?></th><?php } ?>
        <?php if (@$pn4!="") { ?><th class="bg-aqua"><?php echo @$pn4; ?></th><?php } ?>
        <?php if (@$pn5!="") { ?><th class="bg-aqua"><?php echo @$pn5; ?></th><?php } ?>
        <?php if (@$pn6!="") { ?><th class="bg-aqua"><?php echo @$pn6; ?></th><?php } ?>
        <?php if (@$pn7!="") { ?><th class="bg-aqua"><?php echo @$pn7; ?></th><?php } ?>
        <?php if (@$pn8!="") { ?><th class="bg-aqua"><?php echo @$pn8; ?></th><?php } ?>
        <?php if (@$pn9!="") { ?><th class="bg-aqua"><?php echo @$pn9; ?></th><?php } ?>
        <?php if (@$pn10!="") { ?><th class="bg-aqua"><?php echo @$pn10; ?></th><?php } ?>
        <?php if (@$pn11!="") { ?><th class="bg-aqua"><?php echo @$pn11; ?></th><?php } ?>
        <?php if (@$pn12!="") { ?><th class="bg-aqua"><?php echo @$pn12; ?></th><?php } ?>
        <?php if (@$pn13!="") { ?><th class="bg-aqua"><?php echo @$pn13; ?></th><?php } ?>
        <?php if (@$pn14!="") { ?><th class="bg-aqua"><?php echo @$pn14; ?></th><?php } ?>
        <?php if (@$pn15!="") { ?><th class="bg-aqua"><?php echo @$pn15; ?></th><?php } ?>

        <th class="bg-aqua">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    @$tvg=0;
    $tc=0;
    foreach ($resultados as $resultado):
    $tc++;
    @$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9()+$resultado->getVotos10()+$resultado->getVotos11()+$resultado->getVotos12()+$resultado->getVotos13()+$resultado->getVotos14()+$resultado->getVotos15(); 

    @$tvp+=$resultado->getVotos1p($tv)+$resultado->getVotos2p($tv)+$resultado->getVotos3p($tv)+$resultado->getVotos4p($tv)+$resultado->getVotos5p($tv)+$resultado->getVotos6p($tv)+$resultado->getVotos7p($tv)+$resultado->getVotos8p($tv)+$resultado->getVotos9p($tv)+$resultado->getVotos10p($tv)+$resultado->getVotos11p($tv)+$resultado->getVotos12p($tv)+$resultado->getVotos13p($tv)+$resultado->getVotos14p($tv)+$resultado->getVotos15p($tv);

    #@$tvg=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+$pv9+$pv10+$pv12+$pv12;
    @$tvg+=$tv;

    if ($tv==0){
        $tc--;
    }

    @$Votos1pt+=$resultado->getVotos1p($tv);
    @$Votos2pt+=$resultado->getVotos2p($tv);
    @$Votos3pt+=$resultado->getVotos3p($tv);
    @$Votos4pt+=$resultado->getVotos4p($tv);
    @$Votos5pt+=$resultado->getVotos5p($tv);
    @$Votos6pt+=$resultado->getVotos6p($tv);
    @$Votos7pt+=$resultado->getVotos7p($tv);
    @$Votos8pt+=$resultado->getVotos8p($tv);
    @$Votos9pt+=$resultado->getVotos9p($tv);
    @$Votos10pt+=$resultado->getVotos10p($tv);
    @$Votos11pt+=$resultado->getVotos11p($tv);
    @$Votos12pt+=$resultado->getVotos12p($tv);
    @$Votos13pt+=$resultado->getVotos13p($tv);
    @$Votos14pt+=$resultado->getVotos14p($tv);
    @$Votos15pt+=$resultado->getVotos15p($tv);




?>
    <tr>
        <!--
        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEleccion(); ?></td>
    -->
        <td class="bg-gray"><?php echo $resultado->getSeccion(); ?></td>
        <?php if (@$pn1!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos1p($tv); ?></td><?php } ?>
        <?php if (@$pn2!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos2p($tv); ?></td><?php } ?>
        <?php if (@$pn3!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos3p($tv); ?></td><?php } ?>
        <?php if (@$pn4!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos4p($tv); ?></td><?php } ?>
        <?php if (@$pn5!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos5p($tv); ?></td><?php } ?>
        <?php if (@$pn6!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos6p($tv); ?></td><?php } ?>
        <?php if (@$pn7!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos7p($tv); ?></td><?php } ?>
        <?php if (@$pn8!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos8p($tv); ?></td><?php } ?>
        <?php if (@$pn9!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos9p($tv); ?></td><?php } ?>
        <?php if (@$pn10!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos10p($tv); ?></td><?php } ?>
        <?php if (@$pn11!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos11p($tv); ?></td><?php } ?>
        <?php if (@$pn12!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos12p($tv); ?></td><?php } ?>
        <?php if (@$pn13!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos13p($tv); ?></td><?php } ?>
        <?php if (@$pn14!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos14p($tv); ?></td><?php } ?>
        <?php if (@$pn15!="") { ?><td class="bg-gray"><?php echo $resultado->getVotos15p($tv); ?></td><?php } ?>
        <td class="bg-gray"><?php echo "100"; ?> </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="bg-blue" colspan="1">Total</th>
        <?php if (@$pn1!="") { ?><th class="bg-aqua"><?php echo number_format($Votos1pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn2!="") { ?><th class="bg-aqua"><?php echo number_format($Votos2pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn3!="") { ?><th class="bg-aqua"><?php echo number_format($Votos3pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn4!="") { ?><th class="bg-aqua"><?php echo number_format($Votos4pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn5!="") { ?><th class="bg-aqua"><?php echo number_format($Votos5pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn6!="") { ?><th class="bg-aqua"><?php echo number_format($Votos6pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn7!="") { ?><th class="bg-aqua"><?php echo number_format($Votos7pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn8!="") { ?><th class="bg-aqua"><?php echo number_format($Votos8pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn9!="") { ?><th class="bg-aqua"><?php echo number_format($Votos9pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn10!="") { ?><th class="bg-aqua"><?php echo number_format($Votos10pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn11!="") { ?><th class="bg-aqua"><?php echo number_format($Votos11pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn12!="") { ?><th class="bg-aqua"><?php echo number_format($Votos12pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn13!="") { ?><th class="bg-aqua"><?php echo number_format($Votos13pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn14!="") { ?><th class="bg-aqua"><?php echo number_format($Votos14pt/$tc, 2)."%";   ?></th><?php } ?>
        <?php if (@$pn15!="") { ?><th class="bg-aqua"><?php echo number_format($Votos15pt/$tc, 2)."%";   ?></th><?php } ?>
        <th class="bg-aqua"><?php 

        $t= ($Votos1pt/$tc)+($Votos2pt/$tc)+($Votos3pt/$tc)+($Votos4pt/$tc)+($Votos5pt/$tc)+($Votos6pt/$tc)+($Votos7pt/$tc)+($Votos8pt/$tc)+($Votos9pt/$tc)+($Votos10pt/$tc)+($Votos11pt/$tc)+($Votos12pt/$tc)+($Votos13pt/$tc)+($Votos14pt/$tc)+($Votos15pt/$tc);

        echo round($t); ?></th> 
    </tr>
    </tfoot>
</table>





 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart",'bar']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Partido', 'Votos'],
          <?php //foreach ($candidatos as $candidato): ?>
          ['<?php echo strtoupper(@$pn1); ?>',  <?php echo $Votos1pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn2); ?>',  <?php echo $Votos2pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn3); ?>',  <?php echo $Votos3pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn4); ?>',  <?php echo $Votos4pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn5); ?>',  <?php echo $Votos5pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn6); ?>',  <?php echo $Votos6pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn7); ?>',  <?php echo $Votos7pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn8); ?>',  <?php echo $Votos8pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn9); ?>',  <?php echo $Votos9pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn10); ?>',  <?php echo $Votos10pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn11); ?>',  <?php echo $Votos11pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn12); ?>',  <?php echo $Votos12pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn13); ?>',  <?php echo $Votos13pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn14); ?>',  <?php echo $Votos14pt/$tc; ?>],
          ['<?php echo strtoupper(@$pn15); ?>',  <?php echo $Votos15pt/$tc; ?>],
          <?php //endforeach; ?>
        ]);

        var options = {
          title: 'Elecciones',
          is3D: false,
          colors: ['#DF0101', '#0174DF', '#FACC2E', '#ff7800', '#FE9A2E', '#B40404', '#5de5e7', '#A9BCF5', '#9A2EFE', '#9a8c41', '#e6e6e6', '#e6e6e6']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
<br/><br/><br/><br/>

<H1>Elecciones <strong><?php echo  $resultado->getEstado();?> <?php echo $resultado->getEleccion();?><strong></H1>

<?php echo image_tag('logo.png', 'width=200') ?>

    <div id="piechart_3d" style="width: 900px; height: 500px;">

    </div>

<?php
$namefile=$resultado->getEstado()."-".$resultado->getEleccion()."-".date('Y-m-d-H-i-s').".xlsx";
$namefile_path = sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR.$namefile;
#PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->setIncludeCharts(TRUE);
$objWriter->save($namefile_path);



    #$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    #$objWriter->setIncludeCharts(TRUE);
    #$objWriter->save(sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR."plantillacolima1.xlsx");
    #$objWriter->save('php://output');

    #$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
    
    #$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
    #$objWriter->save(sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR."FoodCostDistricts.pdf");


?>


<a href="<?php echo public_path("uploads/".$namefile); ?>">Descargar XLSX</a>

<script type="text/javascript">
$(document).ready( function () {
    $('.datatable').dataTable({
      "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                {
                    "sExtends": "xls",
                    "sTitle": "XLS"
                },
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sTitle": "PDF"
                }
            ]
        },
      aLengthMenu: [
        [10, 25, 100, -1],
        [10, 25, 100, "Todos"]
      ],
      iDisplayLength: 25,
    });

} );
</script>