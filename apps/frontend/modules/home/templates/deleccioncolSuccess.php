    <?php

sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));  
$objPHPExcel = new PHPExcel();  

$inputFileName = sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR.'plantillagob.xlsx';

//  Read your Excel workbook
  try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
  } catch (Exception $e) {
      die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
      . '": ' . $e->getMessage());
  }


//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$fila=2;

$nc_enable=$nc; 


function getpercent($val, $total, $pon=1, $percent=100){
	if ($total==0) return 0;
	return number_format($val/$total*$pon*$percent,2)."%";
}

foreach ($resultados as $resultado){
	if (is_numeric($resultado->getSeccionId())){
		$seccion = SeccionPeer::retrieveByPk($resultado->getSeccionId());
		$id =$seccion->getSeccion();
		$seccionesa[$id]["id"]=$seccion->getId();
		$seccionesa[$id]["seccion"]=$seccion->getSeccion().$seccion->getSubSeccion();
		$seccionesa[$id]["distrito_local"]=$seccion->getDistritoLocal()->getDistritoLocal();
		$seccionesa[$id]["listado_nominal"]=$seccion->getListadoNominal();
		$seccionesa[$id]["ponderacion"]=$seccion->getPonderacion();
		$seccionesa[$id]["pri"]=$resultado->getVotos1();
		$seccionesa[$id]["pan"]=$resultado->getVotos2();
		$seccionesa[$id]["prd"]=$resultado->getVotos3();
		$seccionesa[$id]["mc"]=$resultado->getVotos4();
		$seccionesa[$id]["pes"]=$resultado->getVotos5();
		$seccionesa[$id]["mor"]=$resultado->getVotos6();
		$seccionesa[$id]["otros"]=$resultado->getVotos7();
		$seccionesa[$id]["nu"]=$resultado->getVotos8();
		$seccionesa[$id]["nc"]=$nc_enable ? $resultado->getVotos9() : 0;
		//$seccionesa[$id]["nc"]=$resultado->getVotos8();
		//$seccionesa[$id]["nc"]=0;

		//Suma por Distrito Federal
		$id =$seccion->getDistritoFederal()->getDistritoFederal();
		$dfedral[$id]["id"]=$id;
		$dfedral[$id]["listado_nominal"]=$seccion->getDistritoFederal()->getListadoNominal();
		if ($pv){ 
			$dfedral[$id]["ponderacion"]=$seccion->getDistritoFederal()->getPonderacionVotacion();
		}else{
			$dfedral[$id]["ponderacion"]=$seccion->getDistritoFederal()->getPonderacion();
		}
		@$dfedral[$id]["pri"]+=$resultado->getVotos1();
		@$dfedral[$id]["pan"]+=$resultado->getVotos2();
		@$dfedral[$id]["prd"]+=$resultado->getVotos3();
		@$dfedral[$id]["mc"]+=$resultado->getVotos4();
		@$dfedral[$id]["pes"]+=$resultado->getVotos5();
		@$dfedral[$id]["mor"]+=$resultado->getVotos6();
		@$dfedral[$id]["otros"]+=$resultado->getVotos7();
		@$dfedral[$id]["nu"]+=$resultado->getVotos8();
		@$dfedral[$id]["nc"]+= $nc_enable ? $resultado->getVotos9() : 0;
		//@$dfedral[$id]["nc"]+=$resultado->getVotos8();
		//@$dfedral[$id]["nc"]+=0;
		if ($nc_enable){
			@$dfedral[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9(); 
		}else{
			@$dfedral[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
		}
		//Suma por Distrito Local
		
		$id =$seccion->getDistritoLocal()->getDistritoLocal();
		$dlocal[$id]["id"]=$id;
		$dlocal[$id]["listado_nominal"]=$seccion->getDistritoLocal()->getListadoNominal();
		if ($pv){ 
			$dlocal[$id]["ponderacion"]=$seccion->getDistritoLocal()->getPonderacionVotacion();
		}else{
			$dlocal[$id]["ponderacion"]=$seccion->getDistritoLocal()->getPonderacion();
		}
		//$dlocal[$id]["ponderacion"]=$seccion->getDistritoLocal()->getPonderacion();
		@$dlocal[$id]["pri"]+=$resultado->getVotos1();
		@$dlocal[$id]["pan"]+=$resultado->getVotos2();
		@$dlocal[$id]["prd"]+=$resultado->getVotos3();
		@$dlocal[$id]["mc"]+=$resultado->getVotos4();
		@$dlocal[$id]["pes"]+=$resultado->getVotos5();
		@$dlocal[$id]["mor"]+=$resultado->getVotos6();
		@$dlocal[$id]["otros"]+=$resultado->getVotos7();
		@$dlocal[$id]["nu"]+=$resultado->getVotos8();
		@$dlocal[$id]["nc"]+= $nc_enable ? $resultado->getVotos9() : 0;
		//@$dlocal[$id]["nc"]+=$resultado->getVotos8();
		//@$dlocal[$id]["nc"]+=0;
		if ($nc_enable){
			@$dlocal[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9(); 
		}else{
			@$dlocal[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
		}
		//@$dlocal[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 

		ksort($dlocal);

		//Suma por Municipio
		
		$id =$seccion->getMunicipio();
		$muncipio[$id]["id"]=$id;
		@$muncipio[$id]["pri"]+=$resultado->getVotos1();
		@$muncipio[$id]["pan"]+=$resultado->getVotos2();
		@$muncipio[$id]["prd"]+=$resultado->getVotos3();
		@$muncipio[$id]["mc"]+=$resultado->getVotos4();
		@$muncipio[$id]["pes"]+=$resultado->getVotos5();
		@$muncipio[$id]["mor"]+=$resultado->getVotos6();
		@$muncipio[$id]["otros"]+=$resultado->getVotos7();
		@$muncipio[$id]["nu"]+=$resultado->getVotos8();
		@$muncipio[$id]["nc"]+= $nc_enable ? $resultado->getVotos9() : 0;
		//@$muncipio[$id]["nc"]+=$resultado->getVotos8();
		//@$muncipio[$id]["nc"]+=0;
		if ($nc_enable){
			@$muncipio[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9(); 
		}else{
			@$muncipio[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
		}
		//@$muncipio[$id]["total"]+=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
		//@$muncipio[$id]["total"]+=$resultado->getVotos12();

		ksort($muncipio);

		@$general[0]["pri"]+=$resultado->getVotos1();
		@$general[0]["pan"]+=$resultado->getVotos2();
		@$general[0]["prd"]+=$resultado->getVotos3();
		@$general[0]["mc"]+=$resultado->getVotos4();
		@$general[0]["pes"]+=$resultado->getVotos5();
		@$general[0]["mor"]+=$resultado->getVotos6();
		@$general[0]["otros"]+=$resultado->getVotos7();
		@$general[0]["nu"]+=$resultado->getVotos8();
		@$general[0]["nc"]+= $nc_enable ? $resultado->getVotos9() : 0;
		//@$general[0]["nc"]+=$resultado->getVotos8();
		//@$general[0]["nc"]+=0; 

	}else{
		echo "No exite seccion para: ".$resultado->getSeccion().$seccion->getSubSeccion();
		echo "<br/>";
	}

}

    foreach ($resultados as $resultado){
        if ($resultado->getPartido1()!=""){ @$pn1=$resultado->getPartido1(); }
        if ($resultado->getPartido2()!=""){ @$pn2=$resultado->getPartido2(); }
        if ($resultado->getPartido3()!=""){ @$pn3=$resultado->getPartido3(); }
        if ($resultado->getPartido4()!=""){ @$pn4=$resultado->getPartido4(); }
        if ($resultado->getPartido5()!=""){ @$pn5=$resultado->getPartido5(); }
        if ($resultado->getPartido6()!=""){ @$pn6=$resultado->getPartido6(); }
        if ($resultado->getPartido7()!=""){ @$pn7=$resultado->getPartido7(); }
        if ($resultado->getPartido8()!=""){ @$pn8=$resultado->getPartido8(); }
        if ($resultado->getPartido9()!=""){ @$pn9=$nc_enable ? $resultado->getPartido9() : 0; }
        //if ($resultado->getPartido9()!=""){ @$pn9=$resultado->getPartido9(); }
        //if ($resultado->getPartido10()!=""){ @$pn10=$resultado->getPartido10(); }
        //if ($resultado->getPartido11()!=""){ @$pn11=$resultado->getPartido11(); }
        //if ($resultado->getPartido12()!=""){ @$pn12=$resultado->getPartido12(); }
    }

?>

<H1>Elecciones <strong><?php echo  $resultado->getEstado();?> <?php echo $resultado->getEleccion();?><strong></H1>
<?php
#$objPHPExcel->getActiveSheet()->SetCellValue('D1', $resultado->getEstado()." ".$resultado->getEleccion() ); 
?>
<span class="badge"># votos</span>

<H3>General</H3>

<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <th class="bg-blue"></th>
		<th class="bg-blue">PRI</th>
		<th class="bg-blue">PAN</th>
		<th class="bg-blue">PRD</th>
		<th class="bg-blue">MC</th>
		<th class="bg-blue">PES</th>
		<th class="bg-blue">MOR</th>
		<th class="bg-blue">OTROS</th>
		<th class="bg-blue">NU</th>
		<?php if ($nc_enable){ ?><th class="bg-blue">NC</th><?php } ?>
        <th class="bg-aqua">Total</th>
    </tr>
    </thead>
	<tbody>
    <?php
    foreach ($general as &$votos):
    	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, "G");

    	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $votos['pri']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $votos['pan']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $votos['prd']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $votos['mc']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $votos['pes']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $votos['mor']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $votos['otros']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $votos['nu']);
    	if ($nc_enable){  $objPHPExcel->getActiveSheet()->SetCellValue('L'.$fila, $votos['nc']); }
    ?>
	<tr>
	<th></th>
	<th><span class="badge"><?php echo $votos['pri']?></span></th>
	<th><span class="badge"><?php echo $votos['pan']?></span></th>
	<th><span class="badge"><?php echo $votos['prd']?></span></th>
	<th><span class="badge"><?php echo $votos['mc']?></span></th>
	<th><span class="badge"><?php echo $votos['pes']?></span></th>
	<th><span class="badge"><?php echo $votos['mor']?></span></th>
	<th><span class="badge"><?php echo $votos['otros']?></span></th>
	<th><span class="badge"><?php echo $votos['nu']?></span></th>
	<?php if ($nc_enable){ ?><th><span class="badge"><?php echo $votos['nc']?></span></th><?php } ?>
    <th class="bg-aqua">
    	<?php 
    	$total= $votos['pri']+$votos['pan']+$votos['prd']+$votos['mc']+$votos['pes']+$votos['mor']+$votos['otros']+$votos['nu']+$votos['nc']; 
    	echo $total;
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
	<?php
    foreach ($general as &$votos):
    ?>
	<tr>
	<th></th>
	<th><?php echo getpercent($votos['pri'], $total); ?></th>
	<th><?php echo getpercent($votos['pan'],$total); ?></th>
	<th><?php echo getpercent($votos['prd'],$total); ?></th>
	<th><?php echo getpercent($votos['mc'],$total); ?></th>
	<th><?php echo getpercent($votos['pes'],$total); ?></th>
	<th><?php echo getpercent($votos['mor'],$total); ?></th>
	<th><?php echo getpercent($votos['otros'],$total); ?></th>
	<th><?php echo getpercent($votos['nu'],$total); ?></th>
	<?php if ($nc_enable){ ?><th><?php echo getpercent($votos['nc'],$total); ?></th><?php } ?>
    <th class="bg-aqua">
    	<?php 
    	echo getpercent($total,$total);
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
    </tbody>
   </table>


<H3>DISTRITO FEDERAL</H3>

<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <th class="bg-blue">DF</th>
        <th class="bg-blue">Listado Nom.</th>
        <th class="bg-blue">Pon.</th>
		<th class="bg-blue">PRI</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PAN</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PRD</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MC</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PES</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MOR</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">OTROS</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">NU</th>
		<th class="bg-blue"></th>
		<?php if ($nc_enable){ ?>
		<th class="bg-blue">NC</th>
		<th class="bg-blue"></th>
		<?php } ?>
        <th class="bg-aqua">Total</th>
    </tr>
    </thead>
	<tbody>
    <?php
    $fila=3;

    foreach ($dfedral as &$votos):

    	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, "DF".$votos['id']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, $votos['listado_nominal']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila, $votos['ponderacion']);

    	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $votos['pri']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $votos['pan']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $votos['prd']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $votos['mc']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $votos['pes']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $votos['mor']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $votos['otros']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $votos['nu']);
    	if ($nc_enable){  $objPHPExcel->getActiveSheet()->SetCellValue('L'.$fila, $votos['nc']); }

    	$fila++;

    ?>
	<tr>
	<th class="bg-aqua"><?php echo $votos['id']?></th>
	<th class="bg-aqua"><?php echo $votos['listado_nominal']?></th>
	<th class="bg-aqua"><?php echo $votos['ponderacion']?></th>
	<th><?php echo $t1= getpercent($votos['pri'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pri']?></small><span></th>
	<th><?php echo $t2= getpercent($votos['pan'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pan']?></small><span></th>
	<th><?php echo $t3= getpercent($votos['prd'], $votos['total'], $votos['ponderacion']) ?>  </th><th><span class="badge"><small><?php echo $votos['prd']?></small><span></th>
	<th><?php echo $t4= getpercent($votos['mc'], $votos['total'], $votos['ponderacion']) ?>  </th><th><span class="badge"><small><?php echo $votos['mc']?></small><span></th>
	<th><?php echo $t5= getpercent($votos['pes'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pes']?></small><span></th>
	<th><?php echo $t6= getpercent($votos['mor'], $votos['total'], $votos['ponderacion']) ?>  </th><th><span class="badge"><small><?php echo $votos['mor']?></small><span></th>
	<th><?php echo $t7= getpercent($votos['otros'], $votos['total'], $votos['ponderacion']) ?>  </th><th><span class="badge"><small><?php echo $votos['otros']?></small><span></th>
	<th><?php echo $t8= getpercent($votos['nu'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['nu']?></small><span></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo $t9= getpercent($votos['nc'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['nc']?></small><span></th>
    <?php } ?>
    <th class="bg-aqua">
    	<?php 
    	@$tt1+=$t1;
    	@$tt2+=$t2;
    	@$tt3+=$t3;
    	@$tt4+=$t4;
    	@$tt5+=$t5;
    	@$tt6+=$t6;
    	@$tt7+=$t7;
    	@$tt8+=$t8;
    	@$tt9+=$t9;
    	if ($nc_enable){
    		$total= $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+@$t9; 
    	}else{
    		$total= $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8;
    	}
    	echo $total;
    	@$ttotal+=$total;
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
	<?php
    foreach ($general as &$votos):
    ?>
	<tr>
	<th class="bg-blue"></th>
	<th class="bg-blue"></th>
	<th class="bg-blue">Total</th>
	<th ><?php echo $tt1; ?></th>
	<th></th>
	<th><?php echo $tt2; ?></th>
	<th></th>
	<th><?php echo $tt3; ?></th>
	<th></th>
	<th><?php echo $tt4; ?></th>
	<th></th>
	<th><?php echo $tt5; ?></th>
	<th></th>
	<th><?php echo $tt6; ?></th>
	<th></th>
	<th><?php echo $tt7; ?></th>
	<th></th>
	<th><?php echo $tt8; ?></th>
	<th></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo $tt9; ?></th>
	<th></th>
	<?php } ?>
    <th class="bg-aqua">
    	<?php 
    	echo round($ttotal);
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
    </tbody>
   </table>

<H3>DISTRITO LOCAL</H3>

<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>

        <th class="bg-blue">DL</th>
        <th class="bg-blue">Listado Nom.</th>
        <th class="bg-blue">Pon.</th>
		<th class="bg-blue">PRI</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PAN</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PRD</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MC</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PES</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MOR</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">OTROS</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">NU</th>
		<th class="bg-blue"></th>
		<?php if ($nc_enable){ ?>
		<th class="bg-blue">NC</th>
		<th class="bg-blue"></th>
		<?php } ?>
        <th class="bg-aqua">Total</th>

    </tr>
    </thead>
	<tbody>
    <?php
    	@$tt1=0; @$tt2=0; @$tt3=0; @$tt4=0; @$tt5=0; @$tt6=0; @$tt7=0; @$tt8=0; @$tt9=0;
    	@$ttotal=0;
    	
    foreach ($dlocal as &$votos):

    	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, "DL".$votos['id']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, $votos['listado_nominal']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila, $votos['ponderacion']);

    	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $votos['pri']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $votos['pan']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $votos['prd']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $votos['mc']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $votos['pes']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $votos['mor']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $votos['otros']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $votos['nu']);
    	if ($nc_enable){ $objPHPExcel->getActiveSheet()->SetCellValue('L'.$fila, $votos['nc']); }

    	$fila++;

    ?>
	<tr>
	<th class="bg-aqua"><?php echo $votos['id']?></th>
	<th class="bg-aqua"><?php echo $votos['listado_nominal']?></th>
	<th class="bg-aqua"><?php echo $votos['ponderacion']?></th>
	<th><?php echo $t1= getpercent($votos['pri'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pri']?></small><span></th>
	<th><?php echo $t2= getpercent($votos['pan'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pan']?></small><span></th>
	<th><?php echo $t3= getpercent($votos['prd'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['prd']?></small><span></th>
	<th><?php echo $t4= getpercent($votos['mc'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['mc']?></small><span></th>
	<th><?php echo $t5= getpercent($votos['pes'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['pes']?></small><span></th>
	<th><?php echo $t6= getpercent($votos['mor'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['mor']?></small><span></th>
	<th><?php echo $t7= getpercent($votos['otros'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['otros']?></small><span></th>
	<th><?php echo $t8= getpercent($votos['nu'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['nu']?></small><span></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo $t9= getpercent($votos['nc'], $votos['total'], $votos['ponderacion']) ?> </th><th> <span class="badge"><small><?php echo $votos['nc']?></small><span></th>
    <?php } ?>
    <th class="bg-aqua">
    	<?php 
    	@$tt1+=$t1;
    	@$tt2+=$t2;
    	@$tt3+=$t3;
    	@$tt4+=$t4;
    	@$tt5+=$t5;
    	@$tt6+=$t6;
    	@$tt7+=$t7;
    	@$tt8+=$t8;
    	@$tt9+=$t9;
    	$total= $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+@$t9; 
    	echo $total;
    	@$ttotal+=$total;
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
	<?php
    foreach ($general as &$votos):
    ?>
	<tr>
	<th class="bg-blue"></th>
	<th class="bg-blue"></th>
	<th class="bg-blue">Total</th>
	<th><?php echo $tt1; ?></th>
	<th></th>
	<th><?php echo $tt2; ?></th>
	<th></th>
	<th><?php echo $tt3; ?></th>
	<th></th>
	<th><?php echo $tt4; ?></th>
	<th></th>
	<th><?php echo $tt5; ?></th>
	<th></th>
	<th><?php echo $tt6; ?></th>
	<th></th>
	<th><?php echo $tt7; ?></th>
	<th></th>
	<th><?php echo $tt8; ?></th>
	<th></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo $tt9; ?></th>
	<th></th>
	<?php } ?>
    <th class="bg-aqua">
    	<?php 
    	echo round($ttotal);
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
    </tbody>
   </table>


<H3>Municipio</H3>

<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <th class="bg-blue">Municipio</th>
		<th class="bg-blue">PRI</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PAN</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PRD</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MC</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">PES</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">MOR</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">OTROS</th>
		<th class="bg-blue"></th>
		<th class="bg-blue">NU</th>
		<th class="bg-blue"></th>
		<?php if ($nc_enable){ ?>
		<th class="bg-blue">NC</th>
		<th class="bg-blue"></th>
		<?php } ?>
        <th class="bg-aqua">Total</th>

    </tr>
    </thead>
	<tbody>
    <?php
    	@$tt1=0; @$tt2=0; @$tt3=0; @$tt4=0; @$tt5=0; @$tt6=0; @$tt7=0; @$tt8=0; @$tt9=0;
    	@$ttotal=0;
    	
    foreach ($muncipio as &$votos):

    	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, $votos['id']);
    	#$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, $votos['listado_nominal']);
    	#$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila, $votos['ponderacion']);

    	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $votos['pri']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $votos['pan']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $votos['prd']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $votos['mc']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $votos['pes']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $votos['mor']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $votos['otros']);
    	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $votos['nu']);
    	if ($nc_enable){ $objPHPExcel->getActiveSheet()->SetCellValue('L'.$fila, $votos['nc']); }

    	$fila++;


    ?>
	<tr>
	<th class="bg-aqua"><?php echo $votos['id']?></th>
	<th><?php echo $t1= getpercent($votos['pri'], $votos['total']) ?></th><th> <span class="badge"><small><?php echo $t1= $votos['pri']?></small><span></th>
	<th><?php echo $t2= getpercent($votos['pan'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t2= $votos['pan']?></small><span></th>
	<th><?php echo $t3= getpercent($votos['prd'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t3= $votos['prd']?></small><span></th>
	<th><?php echo $t4= getpercent($votos['mc'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t4= $votos['mc']?></small><span></th>
	<th><?php echo $t5= getpercent($votos['pes'], $votos['total']) ?>  </th><th><span class="badge"><small><?php echo $t5=$votos['pes']?></small><span></th>
	<th><?php echo $t6= getpercent($votos['mor'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t6=$votos['mor']?></small><span></th>
	<th><?php echo $t7= getpercent($votos['otros'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t7=$votos['otros']?></small><span></th>
	<th><?php echo $t8= getpercent($votos['nu'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t8=$votos['nu']?></small><span></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo $t9= getpercent($votos['nc'], $votos['total']) ?> </th><th> <span class="badge"><small><?php echo $t9=$votos['nc']?></small><span></th>
    <?php } ?>
    <th class="bg-aqua">
    	<?php 
    	@$tt1+=$votos['pri'];
    	@$tt2+=$votos['pan'];
    	@$tt3+=$votos['prd'];
    	@$tt4+=$votos['mc'];
    	@$tt5+=$votos['pes'];
    	@$tt6+=$votos['mor'];
    	@$tt7+=$votos['otros'];
    	@$tt8+=$votos['nu'];
    	@$tt9+=$votos['nc'];

    	$total= $t1+$t2+$t3+$t4+$t5+$t6+$t7+$t8+@$t9; 
    	echo round($total);
    	//echo getpercent($total, $total);
    	@$ttotal+=$total;
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
	<?php
    foreach ($general as &$votos):
    ?>
	<tr>
	<th class="bg-blue">Total</th>
	<th><?php echo getpercent($tt1, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt1?></small><span></th>
	<th><?php echo getpercent($tt2, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt2?></small><span></th>
	<th><?php echo getpercent($tt3, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt3?></small><span></th>
	<th><?php echo getpercent($tt4, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt4?></small><span></th>
	<th><?php echo getpercent($tt5, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt5?></small><span></th>
	<th><?php echo getpercent($tt6, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt6?></small><span></th>
	<th><?php echo getpercent($tt7, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt7?></small><span></th>
	<th><?php echo getpercent($tt8, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt8?></small><span></th>
	<?php if ($nc_enable){ ?>
	<th><?php echo getpercent($tt9, $ttotal); ?></th><th><span class="badge"><small><?php echo $tt9?></small><span></th>
    <?php } ?>
    <th class="bg-aqua">
    	<?php 
    	echo getpercent($ttotal, $ttotal);
    	?>
    </th>
	</tr>
	<?php endforeach; ?>
    </tbody>
   </table>



<H3>Votos</H3>
<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <!--
        <th>Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
        -->
        <th class="bg-blue">Seccion</th>
        <th class="bg-aqua"><?php echo @$pn1; ?></th>
        <th class="bg-aqua"><?php echo @$pn2; ?></th>
        <th class="bg-aqua"><?php echo @$pn3; ?></th>
        <th class="bg-aqua"><?php echo @$pn4; ?></th>
        <th class="bg-aqua"><?php echo @$pn5; ?></th>
        <th class="bg-aqua"><?php echo @$pn6; ?></th>
        <th class="bg-aqua"><?php echo @$pn7; ?></th>
        <th class="bg-aqua"><?php echo @$pn8; ?></th>
        <?php if ($nc_enable){ ?>
        <th class="bg-aqua"><?php echo @$pn9; ?></th>
         <?php } ?>
        <th class="bg-aqua">Total</th>
        <th class="bg-aqua"></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $fila=90;
    foreach ($resultados as $resultado):

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, $resultado->getSeccion().$seccion->getSubSeccion());

	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, $resultado->getVotos1());
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila, $resultado->getVotos2());
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila, $resultado->getVotos3());
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila, $resultado->getVotos4());
	//
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila, $resultado->getVotos6());
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila, $resultado->getVotos7());
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $resultado->getVotos8());
	if ($nc_enable){ $objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $resultado->getVotos9()); }
	//$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila, $resultado->getVotos9());
	//$objPHPExcel->getActiveSheet()->SetCellValue('I'.$fila, $resultado->getVotos10());
	//$objPHPExcel->getActiveSheet()->SetCellValue('J'.$fila, $resultado->getVotos11());
	//$objPHPExcel->getActiveSheet()->SetCellValue('K'.$fila, $resultado->getVotos12());

   	$fila++;

    @$pv1+=$resultado->getVotos1();	
  	@$pv2+=$resultado->getVotos2();	
  	@$pv3+=$resultado->getVotos3();	
  	@$pv4+=$resultado->getVotos4();	
  	@$pv5+=$resultado->getVotos5();	
  	@$pv6+=$resultado->getVotos6();	
  	@$pv7+=$resultado->getVotos7();	
  	@$pv8+=$resultado->getVotos8();	
  	@$pv9+=$resultado->getVotos9();	
  	//@$pv10+=$resultado->getVotos10();
    //@$pv11+=$resultado->getVotos11(); 
    //@$pv12+=$resultado->getVotos12();
  	if ($nc_enable){
    	@$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9(); 
    	@$tvg=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+@$pv9;
	}else{
		@$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
    	@$tvg=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8;
	}
		

?>
    <tr>
        <!--
        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEleccion(); ?></td>
        -->
        <td class="bg-gray"><?php echo $resultado->getSeccion().$resultado->getSubSeccion(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos1(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos2(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos3(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos4(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos5(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos6(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos7(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos8(); ?></td>
        <?php if ($nc_enable){ ?>
        <td class="bg-gray"><?php echo $resultado->getVotos9(); ?></td>
        <?php } ?>
        <td class="bg-gray"><?php echo $tv; ?></td>
        <td class="bg-gray"><a href="<?php echo url_for("home/resultado_history?rid=".$resultado->getId()) ?>"><i class="fa fa-eye"></i></a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <!--
        <th class="bg-blue">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
        -->
        <th class="bg-blue">Seccion</th>
        <th class="bg-aqua"><?php echo @$pn1; ?></th>
        <th class="bg-aqua"><?php echo @$pn2; ?></th>
        <th class="bg-aqua"><?php echo @$pn3; ?></th>
        <th class="bg-aqua"><?php echo @$pn4; ?></th>
        <th class="bg-aqua"><?php echo @$pn5; ?></th>
        <th class="bg-aqua"><?php echo @$pn6; ?></th>
        <th class="bg-aqua"><?php echo @$pn7; ?></th>
        <th class="bg-aqua"><?php echo @$pn8; ?></th>
        <?php if ($nc_enable){ ?>
        <th class="bg-aqua"><?php echo @$pn9; ?></th>
        <?php } ?>
        <th class="bg-aqua">Total</th>
        <th class="bg-aqua"></th>
    </tr>
    <tr>
        <th class="bg-blue" colspan="1">Total</th>
        <th class="bg-aqua"><?php echo $pv1; ?></th>
        <th class="bg-aqua"><?php echo $pv2; ?></th>
        <th class="bg-aqua"><?php echo $pv3; ?></th>
        <th class="bg-aqua"><?php echo $pv4; ?></th>
        <th class="bg-aqua"><?php echo $pv5; ?></th>
        <th class="bg-aqua"><?php echo $pv6; ?></th>
        <th class="bg-aqua"><?php echo $pv7; ?></th>
        <th class="bg-aqua"><?php echo $pv8; ?></th>
        <?php if ($nc_enable){ ?>
        <th class="bg-aqua"><?php echo $pv9; ?></th>
        <?php } ?>
        <td class="bg-aqua"><?php echo $tvg; ?></td>
        <th class="bg-aqua"></th>
    </tr>
    </tfoot>
</table>


<H3>% Votos</H3>
<table class="table table-condensed table-hover datatable--">
    <thead>
    <tr>
        <!--
        <th class="bg-blue">Id</th>
        <th class="bg-blue">Estado</th>
        <th class="bg-blue">Eleccion</th>
    -->
        <th class="bg-blue">Seccion</th>
        <th class="bg-aqua"><?php echo @$pn1; ?></th>
        <th class="bg-aqua"><?php echo @$pn2; ?></th>
        <th class="bg-aqua"><?php echo @$pn3; ?></th>
        <th class="bg-aqua"><?php echo @$pn4; ?></th>
        <th class="bg-aqua"><?php echo @$pn5; ?></th>
        <th class="bg-aqua"><?php echo @$pn6; ?></th>
        <th class="bg-aqua"><?php echo @$pn7; ?></th>
        <th class="bg-aqua"><?php echo @$pn8; ?></th>
        <?php if ($nc_enable){ ?>
        <th class="bg-aqua"><?php echo @$pn9; ?></th>
        <?php } ?>
        <th class="bg-aqua">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    @$tvg=0;
    foreach ($resultados as $resultado):
	if ($nc_enable){
    	@$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8()+$resultado->getVotos9(); 
	}else{
		@$tv=$resultado->getVotos1()+$resultado->getVotos2()+$resultado->getVotos3()+$resultado->getVotos4()+$resultado->getVotos5()+$resultado->getVotos6()+$resultado->getVotos7()+$resultado->getVotos8(); 
	}
	//echo $tv;
    #@$tvg=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+$pv9+$pv10+$pv12+$pv12;
	@$tvg+=$tv;


?>
    <tr>
        <!--
        <td class="bg-gray"><?php echo $resultado->getId(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEstado(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getEleccion(); ?></td>
    -->
        <td class="bg-gray"><?php echo $resultado->getSeccion().$resultado->getSubSeccion(); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos1p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos2p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos3p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos4p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos5p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos6p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos7p($tv); ?></td>
        <td class="bg-gray"><?php echo $resultado->getVotos8p($tv); ?></td>
        <?php if ($nc_enable){ ?>
        <td class="bg-gray"><?php echo $resultado->getVotos9p($tv); ?></td>
        <?php } ?>
        <td class="bg-gray"><?php echo $resultado->getSumVotos($tv, 1, $nc_enable)*100; ?>% </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="bg-blue" colspan="1">Total</th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv1, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv2, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv3, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv4, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv5, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv6, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv7, $tvg); ?></th>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv8, $tvg); ?></th>
        <?php if ($nc_enable){ ?>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($pv9, $tvg); ?></th>
        <?php } ?>
        <th class="bg-aqua"><?php echo $resultado->getCalPercent($tvg,$tvg); ?></th>
    </tr>
    </tfoot>
</table>





 <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Partido', 'Votos'],
          <?php //foreach ($candidatos as $candidato): ?>
          ['<?php echo strtoupper(@$pn1); ?>',  <?php echo @$pv1; ?>],
          ['<?php echo strtoupper(@$pn2); ?>',  <?php echo @$pv2; ?>],
          ['<?php echo strtoupper(@$pn3); ?>',  <?php echo @$pv3; ?>],
          ['<?php echo strtoupper(@$pn4); ?>',  <?php echo @$pv4; ?>],
          ['<?php echo strtoupper(@$pn5); ?>',  <?php echo @$pv5; ?>],
          ['<?php echo strtoupper(@$pn6); ?>',  <?php echo @$pv6; ?>],
          ['<?php echo strtoupper(@$pn7); ?>',  <?php echo @$pv7; ?>],
          ['<?php echo strtoupper(@$pn8); ?>',  <?php echo @$pv8; ?>],
          <?php if ($nc_enable){ ?>
          ['<?php echo strtoupper(@$pn9); ?>',  <?php echo @$pv9; ?>],
          <?php } ?>
          //['<?php echo strtoupper(@$pn9); ?>',  <?php echo @$pv9; ?>],
          //['<?php echo strtoupper(@$pn10); ?>',  <?php echo @$pv10; ?>],
          //['<?php echo strtoupper(@$pn11); ?>',  <?php echo @$pv11; ?>],
          //['<?php echo strtoupper(@$pn12); ?>',  <?php echo @$pv12; ?>]
          <?php //endforeach; ?>
        ]);

        var options = {
          title: 'Elecciones',
          is3D: false,
          colors: ['#DF0101', '#0174DF', '#FACC2E', '#FE9A2E','#b66cd8', '#765c2e', '#6b4235','#a3a3a2', '#828180']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
<br/><br/><br/><br/>
    <div id="piechart_3d" style="width: 900px; height: 500px;">

    </div>

<?php
$namefile=$resultado->getEstado()."-".$resultado->getEleccion()."-".date('Y-m-d-H-i-s').".xlsx";
$namefile_path = sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR.$namefile;
#PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
$objWriter->setIncludeCharts(TRUE);
$objWriter->save($namefile_path);



    #$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    #$objWriter->setIncludeCharts(TRUE);
    #$objWriter->save(sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR."plantillacolima1.xlsx");
    #$objWriter->save('php://output');

    #$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
    #$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);
    
    #$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
    #$objWriter->save(sfConfig::get('sf_upload_dir').DIRECTORY_SEPARATOR."FoodCostDistricts.pdf");


?>


<a href="<?php echo public_path("uploads/".$namefile); ?>">Descargar XLSX</a>

<script type="text/javascript">
$(document).ready( function () {
    $('.datatable').dataTable({
      "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                {
                    "sExtends": "xls",
                    "sTitle": "XLS"
                },
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sTitle": "PDF"
                }
            ]
        },
      aLengthMenu: [
        [10, 25, 100, -1],
        [10, 25, 100, "Todos"]
      ],
      iDisplayLength: 25,
    });

} );
</script>