<h1>Sms</h1>

<a class="btn btn-default" href="<?php echo url_for('sms/new') ?>">Agregar SMS</a>

<table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>Sender</th>
      <th>Data</th>
      <th>Processed</th>
      <th>Error log</th>
      <th>Error</th>
      <th>Created at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($smss as $sms): ?>
    <tr>
      <td><a href="<?php echo url_for('sms/edit?id='.$sms->getId()) ?>"><?php echo $sms->getId() ?></a></td>
      <td><?php echo $sms->getSender() ?></td>
      <td><?php echo $sms->getData() ?></td>
      <td><?php echo $sms->getProcessed() ?></td>
      <td><?php echo $sms->getErrorLog() ?></td>
      <td><?php echo $sms->getError() ?></td>
      <td><?php echo $sms->getCreatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  
