<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('sms/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('sms/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'sms/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr class="hide">
        <th><?php echo $form['modem']->renderLabel() ?></th>
        <td>
          <?php echo $form['modem']->renderError() ?>
          <?php echo $form['modem'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th><?php echo $form['sender']->renderLabel() ?></th>
        <td>
          <?php echo $form['sender']->renderError() ?>
          <?php echo $form['sender'] ?>
        </td>
      </tr>
<?php if (!$form->getObject()->isNew()): ?>
      <tr>
        <th>Original</th>
        <td>
          <div class="highlight"><pre><code class="language-html" data-lang="html">
            <?php echo @$sms->getDataOriginal(); ?>
          </code></pre></div>
        </td>
      </tr>
<?php endif; ?>

      <tr>
        <th>Mensaje</th>
        <td>
          <?php echo $form['data']->renderError() ?>
          <?php echo $form['data'] ?>
          <input type="hidden" name="sms[manual_edited]" id="sms_manual_edited" value="true">
        </td>
      </tr>

      
      <tr class="hide">
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
