<?php

/**
 * sms actions.
 *
 * @package    netsales
 * @subpackage sms
 * @author     Jorge Flores
 */
class smsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $c = new Criteria();
    $c->addDescendingOrderByColumn(SmsPeer::ID);
    $this->smss = SmsPeer::doSelect($c);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new smsForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new smsForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($sms = SmsPeer::retrieveByPk($request->getParameter('id')), sprintf('Object sms does not exist (%s).', $request->getParameter('id')));
    $this->form = new smsForm($sms);
    $this->sms = SmsPeer::retrieveByPk($request->getParameter('id'));

  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($sms = SmsPeer::retrieveByPk($request->getParameter('id')), sprintf('Object sms does not exist (%s).', $request->getParameter('id')));
    $this->form = new smsForm($sms);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($sms = SmsPeer::retrieveByPk($request->getParameter('id')), sprintf('Object sms does not exist (%s).', $request->getParameter('id')));
    $sms->delete();

    $this->redirect('sms/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $sms = $form->save();

      if ($sms->getDataOriginal()==""){ 
        $sms->setDataOriginal($sms->getData());
        $sms->save();
      }

      $this->redirect('home/log?id='.$sms->getId());
    }
  }
}
